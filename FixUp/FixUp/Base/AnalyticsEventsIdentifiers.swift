//
//  AnalyticsEventsIdentifiers.swift
//  FixUp
//
//  Created by Camila Pantoja on 5/12/21.
//

import Foundation

let AnalyticsEventActivateFilter = "activate_filter"
let AnalyticsEventVote = "vote"
let AnalyticsEventScreenCreateReport = "screen_create_report"
let AnalyticsEventCreateReportCustomLocation = "create_report_custom_location"
let AnalyticsEventCreateReportPhoto = "create_report_photo"
let AnalyticsEventCreateReport = "create_report"
let AnalyticsEventScreenDashboard = "screen_dashboard"
let AnalyticsEventScreenProfile = "screen_profile"
let AnalyticsEventScreenMap = "screen_map"
let AnalyticsEventScreenNotifications = "screen_notifications"
