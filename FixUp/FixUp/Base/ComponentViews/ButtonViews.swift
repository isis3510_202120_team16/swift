//
//  ButtonViews.swift
//  FixUp
//
//  Created by Camila Pantoja on 13/11/21.
//

import SwiftUI

struct DefaultButton: View {
    // Button label
    let label: String
    // Button action
    let action: () -> Void
    // Button enabling/disabling
    var disabled: Bool
    
    init(label: String, action: @escaping () -> Void, disabled: Bool = false) {
        self.label = label
        self.action = action
        self.disabled = disabled
    }
    
    var body: some View {
        Button(action: action) {
            Text(label)
                .foregroundColor(.white)
                .font(.system(size: 20, weight: .medium))
                .padding(.vertical, 10)
                .frame(maxWidth: .infinity)
                .background(disabled ? Color.gray : Color("PrimaryColor"))
                .cornerRadius(8)
                .padding()
        }.disabled(disabled)
    }
}

struct DetailButton: View {
    var iconSystemName: String
    var action: () -> Void
    var caption: String
    var color: Color
    var disabled: Bool
    
    
    init(iconSystemName: String, action: @escaping () -> Void, caption: String = "", color: Color = Color("PrimaryColor"), disabled: Bool = false) {
        self.iconSystemName = iconSystemName
        self.action = action
        self.caption = caption
        self.color = disabled ? .gray : color
        self.disabled = disabled
    }
    
    var body: some View {
        VStack {
            Button (action: action) {
                Image(systemName: iconSystemName)
                    .foregroundColor(.white)
                    .padding(8)
                    .background(color)
                    .cornerRadius(50)
                    .shadow(radius: 3, x: 1)
            }
            .disabled(disabled)
            if (caption != "") {
                Text(caption)
                    .font(.caption)
                    .foregroundColor(Color(.secondaryLabel))
            }
        }
    }
}
