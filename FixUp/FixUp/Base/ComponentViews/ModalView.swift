//
//  ModalView.swift
//  FixUp
//
//  Created by Camila Pantoja on 6/10/21.
//

import SwiftUI

struct ModalView<Content: View>: View {
    // Variable to hide modal
    @Binding var isShowing: Bool
    // State variable to hide modal on drag
    @State private var offset = CGSize.zero
    
    // Content to be shown in modal
    let content: Content
    // Number between 0 and 1 to set modal screen proportion
    let sizeProp: CGFloat
    
    // Explicit init with @ViewBuilder parameter to support Xcode lower than 12.5
    init(isShowing: Binding<Bool>,sizeProp: CGFloat = 0.85, @ViewBuilder content: () -> Content) {
        self._isShowing = isShowing
        self.content = content()
        self.sizeProp = sizeProp
    }
    
    var body: some View {
        ZStack(alignment: .bottom){
            if isShowing {
                Color.black
                    .opacity(0.3)
                    .ignoresSafeArea()
                    .onTapGesture {
                        isShowing = false
                    }
                VStack {
                    content
                }
                .frame(height: UIScreen.main.bounds.height * self.sizeProp)
                .frame(maxWidth: .infinity)
                .background(
                    ZStack(alignment: .bottom){
                        RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                        Rectangle()
                            .frame(height: UIScreen.main.bounds.height * self.sizeProp / 2)
                    }
                    .foregroundColor(.white)
                    .frame(alignment: .bottom)
                )
                .offset(y: offset.height * 2)
//                .gesture(
//                    DragGesture()
//                        .onChanged { value in
//                            self.offset = value.translation.height > 0 ? value.translation : .zero
//                        }
//                        .onEnded { _ in
//                            if abs(self.offset.height) > 100 {
//                                isShowing = false
//                                self.offset = .zero
//                            } else {
//                                self.offset = .zero
//                            }
//                        }
//                )
                .transition(.move(edge: .bottom))
            }
        }
        .ignoresSafeArea()
        .animation(.easeInOut)
    }
}

struct ModalView_Previews: PreviewProvider {
    static var previews: some View {
        ModalView(isShowing: .constant(true), sizeProp: 0.65){
            Text("Example")
        }
    }
}
