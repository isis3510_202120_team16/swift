//
//  PopUpView.swift
//  FixUp
//
//  Created by Camila Pantoja on 15/11/21.
//

// THIS IS TAKEN FROM https://www.vadimbulavin.com/swiftui-popup-sheet-popover/. BY VADIIM BULAVIN. THIS IS UNLICENSED, FOR FREE USE.

import SwiftUI


struct PopupView<T: View>: ViewModifier {
    let popup: T
    let isPresented: Bool
    let alignment: Alignment
    let direction: Direction
    
    init(isPresented: Bool, alignment: Alignment, direction: Direction, @ViewBuilder content: () -> T) {
        self.isPresented = isPresented
        self.alignment = alignment
        self.direction = direction
        popup = content()
    }

    func body(content: Content) -> some View {
        content
            .overlay(popupContent())
    }

    @ViewBuilder private func popupContent() -> some View {
        GeometryReader { geometry in
            if isPresented {
                popup
                    .animation(.spring())
                    .transition(.offset(x: 0, y: direction.offset(popupFrame: geometry.frame(in: .global))))
                    .frame(width: geometry.size.width, height: geometry.size.height, alignment: alignment)
            }
        }
    }
}

extension PopupView {
    enum Direction {
        case top, bottom

        func offset(popupFrame: CGRect) -> CGFloat {
            switch self {
            case .top:
                let aboveScreenEdge = -popupFrame.maxY
                return aboveScreenEdge
            case .bottom:
                let belowScreenEdge = UIScreen.main.bounds.height - popupFrame.minY
                return belowScreenEdge
            }
        }
    }
}

struct PopUpView_Previews: PreviewProvider {
    static var previews: some View {
        Preview()
    }
    // Helper view that shows a popup
    struct Preview: View {
        @State var isPresented = false
        
        var body: some View {
            ZStack {
                Color.clear
                VStack {
                    Button("Toggle", action: { isPresented.toggle() })
                    Spacer()
                }
            }
            .modifier(PopupView(isPresented: isPresented,
                            alignment: .top,
                            direction: .top,
                            content: { Color.yellow.frame(width: 100, height: 100) }))
        }
    }
}

private extension GeometryProxy {
    var belowScreenEdge: CGFloat {
        UIScreen.main.bounds.height - frame(in: .global).minY
    }
}
