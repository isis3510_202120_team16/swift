//
//  StorageManager.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

import SwiftUI
import Firebase
import Combine

public class StorageManager: ObservableObject {
    let storage = Storage.storage()
    
    /**
     * Uploads image to Firebase images bucket
     * Returns download url
     */
    func uploadImage(image: UIImage, fileName: String? = nil) -> Future<URL, Error> {
        // Create random file name if noo fileName
        let fileName = fileName ?? UUID().uuidString;
        
        // Create path for image
        let filePath = "images/\(fileName).jpg"
        
        // Create a storage reference
        let storageRef = storage.reference().child(filePath)
        
        // Convert the image into JPEG and compress the quality to reduce its size
        let data = image.jpegData(compressionQuality: 0.8)
        
        // Change the content type to jpg. If you don't, it'll be saved as application/octet-stream type
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpg"
        
        // Convert upload call to promise
        return Future { promise in
            storageRef.putData(data!, metadata: metadata) { (metadata, error) in
                guard let metadata = metadata else {
                    promise(.failure(error!))
                    return
                }

                print("Success, metadata: \(metadata)")
                // Extract download URL from reference
                storageRef.downloadURL { (url, error) in
                    if let error = error {
                        promise(.failure(error))
                    }
                    
                    if let url = url{
                        promise(.success(url))
                    }
                }
            }
        }
    }
    
    func downloadImage (absolutePath: String) -> Future<UIImage, Error> {
        print("Downloading image in thread \(Thread.current)")
        // Create a reference to the file you want to download
        let islandRef = storage.reference(forURL: absolutePath)
        
        return Future { promise in
            islandRef.getData(maxSize: 8 * 1024 * 1024) { (data, error) in
                if let error = error {
                    promise(.failure(error))
                } else {
                    promise(.success(UIImage(data: data!)!))
                }
            }
        }
    }
}

enum FBStorageError: Error {
    case uploadError
}
