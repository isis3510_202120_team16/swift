//
//  ContentView.swift
//  FixUp
//
//  Created by Juan David Serrano on 5/10/21.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            if loginViewModel.signedIn {
                TabBarView()
            }
            else {
                //LoginView()
                AuthView()
            }
        }
        .onAppear{
            loginViewModel.signedIn = loginViewModel.isSignedIn
        }
        .addModal()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
