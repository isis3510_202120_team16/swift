//
//  FixUpApp.swift
//  FixUp
//
//  Created by Juan David Serrano on 5/10/21.
//

import SwiftUI
import Firebase
import GoogleMaps

@main
struct FixUpApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            let loginViewModel = LoginViewModel()
            let modalManager: ModalManager = ModalManager()
            ContentView()
                .environmentObject(loginViewModel)
                .environmentObject(modalManager)
        }
    }
}

class AppDelegate: NSObject, UIApplicationDelegate {
    internal func application(_ application: UIApplication,
                              didFinishLaunchingWithOptions launchOptions:
                                [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyC_ZKZnRYk1V2WYWxHMsxsaz1HLWUomJs0")
        
        return true
    }
}
