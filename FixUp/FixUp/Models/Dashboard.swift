//
//  Dashboard.swift
//  FixUp
//
//  Created by Juan David Serrano on 8/10/21.
//

struct Dashboard: Codable{
    var reportsMade: Double
    var daysSinceLastReport: Double
    var repairPercentage: Double
    var averageRepairTime: Double
}
