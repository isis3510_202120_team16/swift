//
//  Location.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

struct Location: Codable, Hashable {
    var latitude: Double
    var longitude: Double
}

