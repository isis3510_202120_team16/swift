//
//  Media.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

struct Media: Codable, Hashable {
    var altName: String
    var path: String
    var type: MediaType
}

enum MediaType: String, Codable, Hashable  {
    case PHOTO = "PHOTO"
    case VIDEO = "VIDEO"
}
