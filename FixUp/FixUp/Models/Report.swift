//
//  Report.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

import Foundation
import FirebaseFirestoreSwift

struct Report: Codable, Hashable {
    var id: String? // In theory, id should be @DocumentID but this fails with CLOUD FUNCTIONS
    var author: String?
    var category: ReportCategory
    var city: String?
    var country: String?
    var created: Date? // This is changed to Timestamp, don't worry
    var creationLocation: Location?
    var damageLocation: Location
    var description: String?
    var media: Media?
    var numVotes: Int?
    var severity: Int?
    var status: ReportStatus?
    var type: ReportType?
    var timer: Int?
    
    // These keys are safe to DECODE from CLOUD FUNCTIONS
    enum CodingKeys: String, CodingKey {
        case id
        case author
        case category
        case city
        case country
        case creationLocation
        case damageLocation
        case description
        case media
        case numVotes
        case severity
        case status
        case type
        case timer
    }
    
    // These keys are needed to ENCODE for FIRESTORE
    enum AdditionalEncodingKeys: String, CodingKey {
        case created
    }
    
    // Everything is encoded, except for id because this is automatically managed by Firestore.
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if id != nil{
            try container.encode(id, forKey: .id)
        }
        try container.encode(author, forKey: .author)
        try container.encode(category, forKey: .category)
        try container.encode(city, forKey: .city)
        try container.encode(country, forKey: .country)
        try container.encode(creationLocation, forKey: .creationLocation)
        try container.encode(damageLocation, forKey: .damageLocation)
        try container.encode(description, forKey: .description)
        try container.encode(media, forKey: .media)
        try container.encode(numVotes, forKey: .numVotes)
        try container.encode(severity, forKey: .severity)
        try container.encode(status, forKey: .status)
        try container.encode(type, forKey: .type)
        try container.encode(timer, forKey: .timer)
        
        var additionalInfo = encoder.container(keyedBy: AdditionalEncodingKeys.self)
        try additionalInfo.encode(created, forKey: .created)
    }
}


struct Created: Codable, Hashable {
    var _nanoseconds: Int
    var _seconds: Int
    
    static func from(date: Date) -> Created {
        let nowInSeconds = date.timeIntervalSince1970
        return Created(_nanoseconds: Int((nowInSeconds - floor(nowInSeconds)) * 1000000000) , _seconds: Int(floor(nowInSeconds)))
    }
}


enum ReportCategory: String, Codable, CaseIterable, Hashable {
    case Sidewalk = "Sidewalk"
    case Road = "Road"
    case Vandalism = "Vandalism"
    
    func getColorKey() -> String {
        switch self {
        case .Road:
            return "RoadColor"
        case .Sidewalk:
            return "SidewalkColor"
        case .Vandalism:
            return "VandalismColor"
        }
    }
}

enum ReportType: String, Codable, CaseIterable, Hashable  {
    case Damage = "Damage"
    case Suggestion = "Suggestion"
}

enum ReportStatus: String, Codable, CaseIterable, Hashable  {
    case Active = "Active"
    case InProgress = "InProgress"
    case Repaired = "Repaired"
    
    func getDetailMeaning() -> String {
        switch self {
        case .Active:
            return "Pending repair"
        case .InProgress:
            return "Repair in progress"
        case .Repaired:
            return "Repaired"
        }
    }
}
