//
//  ReportImage.swift
//  FixUp
//
//  Created by Juan David Serrano on 14/11/21.
//

import Foundation
import SwiftUI

struct ReportImage: Hashable, Identifiable {
    var id = UUID()
    var report: Report
    var image: UIImage
}
