//
//  UsersVotes.swift
//  FixUp
//
//  Created by Daniela González on 14/11/21.
//

import Foundation
import FirebaseFirestoreSwift

struct UsersVotes: Codable {
    var id: String?
    var votedReports: [String]
    
}
