//
//  ReportRepository.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

import FirebaseFirestore
import FirebaseFirestoreSwift
import Combine

final class ReportRepository: ObservableObject {
    private let path = "reports"
    private let store = Firestore.firestore()
    @Published var reports: [Report] = []
    private let networkMonitor = NetworkMonitor.singleton
    private let cacheManager = ReportCacheManager.instance
    private let imageCacheManager = ImageCacheManager.instance
    private let CACHED_DRAFT_KEY = "CACHED_DRAFT";
    private let IMAGE_DRAFT_KEY = "CACHED_DRAFT_IMAGE";
    
    init(){
        getAll()
    }
    
    func getAll() {
        store.collection(path).addSnapshotListener{(snapshot, error) in
            if let error = error {
                print(error)
                return
            }
            self.reports = snapshot?.documents.compactMap{
                try? $0.data(as: Report.self)
            } ?? []
        }
    }
    
    func add(_ report: Report) throws {
        if !networkMonitor.isConnected {
            saveDraft(report: report)
            throw CreateReportError.noConnection
        }
        do {
            _ = try store.collection(path).addDocument(from: report)
            // Clean cached draft
            cleanDraft()
        } catch {
            throw CreateReportError.unexpected(message: error.localizedDescription)
        }
    }
    
    func addVoteUser( report: Report){
        
        let reportDoc = store.collection(path).document(report.id!)
        
        reportDoc.updateData(["numVotes": FieldValue.increment(Int64(1))])
        
    }
    
    func saveDraft(report: Report, image: UIImage? = nil) {
        cacheManager.add(key: CACHED_DRAFT_KEY, value: report)
        if let unwrappedImage = image {
            imageCacheManager.add(key: IMAGE_DRAFT_KEY, value: unwrappedImage)
        }
    }
    
    func retrieveDraft() -> (Report?, UIImage?) {
        return (cacheManager.get(key: CACHED_DRAFT_KEY), imageCacheManager.get(key: IMAGE_DRAFT_KEY))
    }
    
    func cleanDraft() {
        cacheManager.remove(key: CACHED_DRAFT_KEY)
        cacheManager.remove(key: IMAGE_DRAFT_KEY)
    }
    
}


enum CreateReportError: Error {
    // Throw when an invalid password is entered
    case noConnection
    
    // Error with image upload
    case imageUpload

    // Throw in all other cases
    case unexpected(message: String)
}

extension CreateReportError: CustomStringConvertible {
    public var description: String {
        switch self {
        case .noConnection:
            return "No connection"
        case .unexpected(_):
            return "Unexpected error"
        case .imageUpload:
            return "Image wasn't uploaded"
        }
    }
}
