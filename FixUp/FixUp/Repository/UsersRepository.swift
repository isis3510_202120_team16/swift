//
//  UsersRepository.swift
//  FixUp
//
//  Created by Daniela González on 14/11/21.
//

import FirebaseFirestore
import FirebaseFirestoreSwift
import Combine

final class UsersRepository: ObservableObject {
    private let path = "users"
    private let store = Firestore.firestore()
    @Published var usersVotes: [UsersVotes] = []
    
    init(){
        getAll()
    }

    func getAll() {
        store.collection(path).addSnapshotListener{(snapshot, error) in
            if let error = error {
                print(error)
                return
            }
            
            //print(snapshot!.documents[0].documentID)
            self.usersVotes = snapshot?.documents.compactMap{
                try? $0.data(as: UsersVotes.self)
            } ?? []
            
            for(index, element) in snapshot!.documents.enumerated(){
                self.usersVotes[index].id = element.documentID
            }
        }
    }
    
    func add(usersVote: UsersVotes) throws {
        
        let docData: [String: Any] = [
            "votedReports": usersVote.votedReports
        ]
        
        let docId = usersVote.id
        
        store.collection(path).document(docId!).setData(docData){ err in
            if let err = err {
                print("Error writing document: \(err)")
            } else {
                print("UsersVotes successfully written!")
            }
        }
    }
    
    func update(userId: String, reportId: String) throws {
        
        let usersDoc = store.collection(path).document(userId)
        //var tempvotedReports: [String] = []
        
        usersDoc.updateData(["votedReports": FieldValue.arrayUnion([reportId])]){ err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document successfully updated")
            }
        }
        
        /*usersDoc.getDocument { (document, error) in
            if let document = document, let data = document.data(){
                if var votedReports = data["votedReports"] as? [String] {
                    print(votedReports)
                    votedReports.append(reportId)
                    tempvotedReports = votedReports
                    print(tempvotedReports)
                    
                    usersDoc.updateData(["votedReports": tempvotedReports]){ err in
                        if let err = err {
                            print("Error updating document: \(err)")
                        } else {
                            print("Document successfully updated")
                        }
                    }
                }
            } else {
                print("Document does not exist")
            }
        }*/
    }
}

