//
//  ImageCacheManager.swift
//  FixUp
//
//  Created by Camila Pantoja on 15/11/21.
//

import Foundation
import SwiftUI

class ImageCacheManager {
    static let instance = ImageCacheManager()
    private init(){ }
    
    var reportCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        cache.countLimit = 30
        cache.totalCostLimit = 1024 * 1024 * 30;
        return cache
    }()
    
    func add(key: String, value: UIImage) {
        reportCache.setObject(value, forKey: key as NSString)
    }
    
    func get(key: String) -> UIImage? {
        return reportCache.object(forKey: key as NSString)
    }
    
    func remove(key: String) {
        reportCache.removeObject(forKey: key as NSString)
    }
}
