//
//  LocationManager.swift
//  FixUp
//
//  Created by Juan David Serrano on 5/10/21.
//

import MapKit

enum MapDetails{
    static let defaultLocation = CLLocationCoordinate2D(latitude: 4.60306, longitude: -74.06574)
    static let defaultSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
}

class LocationManager: NSObject, ObservableObject, CLLocationManagerDelegate{
    
    static let singleton = LocationManager()
    
    @Published var region: MKCoordinateRegion?
    
    var clLocationManager: CLLocationManager?
    
    var reportingLocation : Bool = false
    
    var consecutiveMismatch = 0
    
    private override init() {
//        self.region = MKCoordinateRegion(center:MapDetails.defaultLocation , span: MapDetails.defaultSpan)
        
    }
    
    func checkIfLocationServicesIsEnabled(){
        if CLLocationManager.locationServicesEnabled(){
            clLocationManager = CLLocationManager()
            clLocationManager!.delegate = self
            clLocationManager!.desiredAccuracy = 50
            clLocationManager!.distanceFilter = kCLLocationAccuracyNearestTenMeters
        }else{
            print("Show alert location services are not enabled and they need to be: Not Enabled")
        }
    }
    
    
    private func checkLocationAuthorization(){
        guard let clLocationManager = clLocationManager else {return}
        switch  clLocationManager.authorizationStatus {
        case .notDetermined:
            clLocationManager.requestWhenInUseAuthorization()
        case .restricted, .denied:
            if reportingLocation{
                reportingLocation.toggle()
                clLocationManager.stopUpdatingLocation()
            }
            print("Show alert location services are not enabled and they need to be: Restricted")
        case .authorizedAlways, .authorizedWhenInUse:
            if !reportingLocation{
                reportingLocation.toggle()
                clLocationManager.startUpdatingLocation()
            }
        @unknown default:
            break
        }
        
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        checkLocationAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if region == nil && locations.last != nil{
            region =  MKCoordinateRegion(center:locations.last!.coordinate , span: MapDetails.defaultSpan)
        }else if locations.last != nil{
            let new_location_helper = CLLocation(latitude: locations.last!.coordinate.latitude, longitude: locations.last!.coordinate.longitude)
            let current_location_helper = CLLocation(latitude: region!.center.latitude, longitude: region!.center.longitude)
            print("distance (m)", new_location_helper.distance(from: current_location_helper))
            if new_location_helper.distance(from: current_location_helper) > 25.0{
                if consecutiveMismatch > 0{
                    region = MKCoordinateRegion(center:locations.last!.coordinate , span: MapDetails.defaultSpan)
                    consecutiveMismatch = 0
                    print("location updated")
                }else{
                    consecutiveMismatch+=1
                }
                
            }
            
        }
    }
}
