//
//  ModalManager.swift
//  FixUp
//
//  Created by Camila Pantoja on 11/11/21.
//

import SwiftUI

public class ModalManager: ObservableObject {
    // Variable to hide or present modal
    @Published var isPresented: Bool = false {
        didSet {
            if !isPresented {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.35) { [weak self] in
                    self?.content = AnyView(EmptyView())
                }
            }
        }
    }
    
    // Modal content
    @Published private(set) var content: AnyView
    
    public var defaultAnimation: Animation = .interpolatingSpring(stiffness: 300.0, damping: 30.0, initialVelocity: 10.0)
    
    public init() {
        self.content = AnyView(EmptyView())
    }
    
    /**
     Presents a Modal that presents content
     */
    public func showModal<T: View>(@ViewBuilder content: @escaping () -> T){
        guard !isPresented else {
            withAnimation(defaultAnimation) {
                updateModal(
                    content: {
                        // do not animate the content, just the partial sheet
                        withAnimation(nil) {
                            content()
                        }
                    })
            }
            return
        }
        
        self.content = AnyView(content())
        DispatchQueue.main.async {
            withAnimation(self.defaultAnimation) {
                self.isPresented = true
            }
        }
    }
    
    /**
     Updates some properties of the modal
     */
    public func updateModal<T:View>(isPresented: Bool? = nil, content: (() -> T)? = nil){
        if let content = content {
            self.content = AnyView(content())
        }
        
        if let isPresented = isPresented {
            withAnimation(defaultAnimation) {
                self.isPresented = isPresented
            }
        }
    }
    /**
     Closes modal
     */
    public func closeModal() {
        withAnimation(defaultAnimation) {
            self.isPresented = false
        }
    }
}
