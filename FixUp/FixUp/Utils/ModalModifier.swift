//
//  ModalModifier.swift
//  FixUp
//
//  Created by Camila Pantoja on 11/11/21.
//

import SwiftUI

struct ModalHalf: ViewModifier {
    // Modal manager
    @EnvironmentObject private var manager: ModalManager
    // The offset for the drag gesture
    @State private var offset = CGSize.zero
    // Number between 0 and 1 to set modal screen proportion
    let sizeProp: CGFloat = 0.7
    
    func body(content: Content) -> some View {
        ZStack {
            content
            
            ZStack (alignment: .bottom) {
                if manager.isPresented {
                    Color.black
                        .opacity(0.3)
                        .ignoresSafeArea()
                        .onTapGesture {
                            withAnimation(manager.defaultAnimation) {
                                self.manager.isPresented = false
                            }
                        }
                    
                    VStack {
                        self.manager.content
                    }
                    .frame(height: UIScreen.main.bounds.height * sizeProp)
                    .frame(maxWidth: .infinity)
                    .shadow(color: Color(.sRGBLinear, white: 0, opacity: 0.13), radius: 10.0)
                    .background(
                        ZStack(alignment: .bottom){
                            RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                            Rectangle()
                                .frame(height: UIScreen.main.bounds.height * sizeProp / 2)
                        }
                        .foregroundColor(Color(UIColor.systemBackground))
                        .frame(alignment: .bottom)
                    )
                    .offset(y: self.offset.height * 2)
                    .gesture(
                        DragGesture()
                            .onChanged { value in
                                self.offset = value.translation.height > 0 ? value.translation : .zero
                            }
                            .onEnded { _ in
                                if abs(self.offset.height) > UIScreen.main.bounds.height * 0.3 {
                                    self.manager.isPresented = false
                                    self.offset = .zero
                                } else {
                                    self.offset = .zero
                                }
                            }
                    )
                    .transition(.move(edge: .bottom))
                }
            }
            .edgesIgnoringSafeArea(.vertical)
            
        }
    }
}


struct ModalAddView<Base: View, InnerContent: View>: View {
    @EnvironmentObject var modalManager: ModalManager
    
    @Binding var isPresented: Bool
    let content: () -> InnerContent
    let base: Base
    
    var body: some View {
        return AnyView(base
                        .onChange(of: isPresented, perform: {_ in updateContent() }))
        
    }
    
    func updateContent() {
        modalManager.updateModal(isPresented: isPresented, content: content)
    }
    
}

public extension View {
    func modalHalf<Content: View>(isPresented: Binding<Bool>, @ViewBuilder content: @escaping () -> Content) -> some View {
        ModalAddView(isPresented: isPresented, content: content, base: self)
    }
}

extension View {
    public func addModal() -> some View {
        self.modifier(
            ModalHalf()
        )
    }
}
