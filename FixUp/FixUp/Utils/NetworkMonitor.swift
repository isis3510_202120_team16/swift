//
//  NetworkMonitor.swift
//  FixUp
//
//  Created by Daniela González on 12/11/21.
//

import Foundation
import Network

final class NetworkMonitor: ObservableObject {
    
    static let singleton = NetworkMonitor()
    
    let monitor = NWPathMonitor()
    
    let queue = DispatchQueue(label: "Monitor")
    
    @Published
    var isConnected = true
    
    private init(){
        monitor.pathUpdateHandler = { [self] path in
            DispatchQueue.main.async {
                self.isConnected = path.status != .unsatisfied
                print(self.isConnected)
            }
        }
        monitor.start(queue: queue)
    }
    
}
