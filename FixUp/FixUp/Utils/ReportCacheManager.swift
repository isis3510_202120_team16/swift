//
//  ReportCacheManager.swift
//  FixUp
//
//  Created by Camila Pantoja on 12/11/21.
//

import Foundation

class ReportCacheManager {
    static let instance = ReportCacheManager()
    private init(){ }
    
    var reportCache: NSCache<NSString, StructWrapper<Report>> = {
        let cache = NSCache<NSString, StructWrapper<Report>>()
        cache.countLimit = 30
        cache.totalCostLimit = 1024 * 1024 * 15;
        return cache
    }()
    
    func add(key: String, value: Report) {
        reportCache.setObject(StructWrapper(value), forKey: key as NSString)
    }
    
    func get(key: String) -> Report? {
        return reportCache.object(forKey: key as NSString)?.value
    }
    
    func remove(key: String) {
        reportCache.removeObject(forKey: key as NSString)
    }
}

class StructWrapper<T>: NSObject {
    
    let value: T
    
    init(_ _struct: T) {
        self.value = _struct
    }
}
