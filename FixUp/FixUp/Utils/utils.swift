//
//  utils.swift
//  FixUp
//
//  Created by Camila Pantoja on 13/11/21.
//

import SwiftUI

extension Binding {
    static func ??(lhs: Binding<Optional<Value>>, rhs: Value) -> Binding<Value> {
        return Binding(get: { lhs.wrappedValue ?? rhs }, set: { lhs.wrappedValue = $0 })
    }
}
