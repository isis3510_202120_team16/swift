//
//  CreateReportViewModel.swift
//  FixUp
//
//  Created by Camila Pantoja on 7/10/21.
//

import Combine
import SwiftUI
import FirebaseAuth
import Firebase

final class CreateReportViewModel: ObservableObject {
    // Report repository for data access
    var reportRepository = ReportRepository()
    
    // Report to be created
    @Published var report: Report
    
    // Image to display
    @Published var image: Image? = Image(systemName: "plus")
    
    // Image for report to be created
    @Published var uiImage: UIImage?
    
    // Timer for analytics
    @Published var initialTime: Date;
    
    // Published error if any
    @Published var error: Error?;
    
    // Uploading status
    @Published var uploadStatus = UploadStatus.notStarted
    
    // Controls if snackbar with status should be shown
    @Published var shouldPresentSnackbar = false
    
    // Cancellables for Combine
    private var cancellables: Set<AnyCancellable> = []
    
    // Network Monitor to check for internet connection, not subscribed
    let networkMonitor = NetworkMonitor.singleton
    
    /**
     Initializes report with cached form if any,  to default values otherwise
     */
    init(initLat: Double, initLong: Double) {
        // Initialize timer for analytics
        self.initialTime = Date()
        
        // Attempt to retrieve a saved draft
        let (draftReport, draftImage) = reportRepository.retrieveDraft()
        if let draftReport = draftReport {
            // If draft exists, initialize the report with saved draft
            report = draftReport
            // If image exists, initialize ui image and image so they can be displayed
            if let draftImage = draftImage {
                uiImage = draftImage
                image = Image(uiImage: draftImage) // Maybe this could be done in View... TODO: think about it
            }
            uploadStatus = UploadStatus.pendingDraft
            shouldPresentSnackbar = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                self.shouldPresentSnackbar = false
            }
        } else {
            // If draft doesn't exist, initialize report with default values
            self.report = CreateReportViewModel.resettedReport(initialLocation: Location(latitude: initLat, longitude: initLong))
        }
    }
    
    /**
     Returns a report reset to default values
     */
    static func resettedReport(initialLocation: Location) -> Report {
        // Create default report
        let user = Auth.auth().currentUser
        var userId: String = ""
        if let user = user {
            userId = user.uid
        }
        
        return Report( author: userId, category: ReportCategory.Road, city: "Bogota", country: "Colombia", damageLocation: initialLocation, description: "", numVotes: 0, severity: 1, status: ReportStatus.Active, type: ReportType.Damage)
    }
    
    /**
     Calculates time in seconds, rounded to nearest second, since initialTime
     */
    private func calculateTimer() -> Int {
        return -Int(round(initialTime.timeIntervalSinceNow))
    }
    
    func addReport( createdLocation: Location ) {
        self.report.timer = calculateTimer()
        self.report.creationLocation = createdLocation;
                
        // Handle offline mode
        guard networkMonitor.isConnected else {
            // Update error to show in UI
            error = CreateReportError.noConnection
            uploadStatus = UploadStatus.connectionError

            // Save draft
            reportRepository.saveDraft(report: report, image: uiImage)
            return
        }
        
        uploadStatus = UploadStatus.isUploading
        
        // Perform uploading tasks in another thread
        DispatchQueue.global(qos: .utility).async { [self] in
            // If photo had not been uploaded before, upload it
            if !(self.report.media?.path != nil) {
                let storageManager = StorageManager()
                guard let unwrappedImage = self.uiImage else { return }
                
                storageManager.uploadImage(image: unwrappedImage)
                    .sink { completion in
                        
                        switch completion {
                        case.finished:
                            break
                        case.failure(let error):
                            DispatchQueue.main.async {
                                // Update error to show in UI
                                self.error = error
                                self.uploadStatus = UploadStatus.connectionError
                            }
                            // Save draft
                            reportRepository.saveDraft(report: self.report, image: uiImage)
                            // Stop function execution
                            return
                        }
                    } receiveValue: { url in
                        DispatchQueue.main.async {
                            // Assign report media url and creation timestamp
                            self.report.media = Media(
                                altName: "Photo report of type \(self.report.category)",
                                path: url.absoluteString,
                                type: MediaType.PHOTO
                            )
//                            self.report.created = Created.from(date: Date())
                            self.report.created = Date()
                            
                            
                            // TIME TO SAVE REPORT -- in main because of sync issues
                            guard self.report.media?.path != nil else {
                                    error = CreateReportError.imageUpload
                                    uploadStatus = UploadStatus.connectionError
                                return
                            }
                            // Add report to db using repository
                            do {
                                // Attempt to add to db. If sucessful, the repository cleans cache
                                try self.reportRepository.add(self.report)
                                // Firebase Event
                                Analytics.logEvent(AnalyticsEventCreateReport, parameters: nil)
                                // If successful, update status and reset draft to default values
                                self.uploadStatus = UploadStatus.completed
                                report = CreateReportViewModel.resettedReport(initialLocation: createdLocation)
                                image = Image(systemName: "plus")
                                uiImage = nil
                                error = nil
                            } catch CreateReportError.noConnection {
                                // Update error to show in UI... draft is already being saved
                                self.uploadStatus = UploadStatus.connectionErrorFBinvoked
                                error = CreateReportError.noConnection
                            } catch {
                                self.error = error
                                self.uploadStatus = UploadStatus.connectionErrorFBinvoked
                                // Save draft
                                reportRepository.saveDraft(report: report)
                            }
                            
                            
                        }
                        
                    }
                    .store(in: &self.cancellables)
            }

        }
        return
    }
}

enum UploadStatus: String {
    case notStarted = ""
    case isUploading = "Report is being created"
    case completed = "Report created successfully"
    case connectionError = "Pending draft was saved due to connectivity issues. Try again later."
    case connectionErrorFBinvoked = "Report creation failed, will resume when connection is restored."
    case pendingDraft = "Pending draft was loaded"
}
