//
//  DashboardViewModel.swift
//  FixUp
//
//  Created by Daniela González on 8/10/21.
//

import FirebaseFunctions
import FirebaseAuth
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import MapKit
import Foundation
import Combine
import CoreLocation

class DashboardViewModel: ObservableObject {
    
    @Published
    var reports: [ReportImage] = []
    
    @Published
    var images: [String: String] = [:]
    
    @Published
    var userReportsStats: [String: String] = [:]
    
    @Published
    var overallStats: [String: String] = [:]
    
    @Published
    var userVotesStats: [String: String] = [:]
    
    @Published
    var city: String = "Bogota"
    
    private var cancellables: Set<AnyCancellable> = []
    
    let locationManager: LocationManager = LocationManager.singleton
    
    let networkMonitor: NetworkMonitor = NetworkMonitor.singleton

    static let cities: [String] = [ "Bogota", "Cali", "Cartagena"]
    
    private let uuid = Auth.auth().currentUser!.uid

    lazy var functions = Functions.functions()
    
    private let db = Firestore.firestore()
    
    let defaults = UserDefaults.standard
    
    static let mapping = ["reportsMade": "Reports Made", "daysSinceLastReport": "Days Since Last Report", "repairPercentage": "Repaired(%)", "averageRepairTime": "Average Days to Repair", "averageVotes": "Average votes per repair", "reportsVoted": "Voted Reports", "category1": "Most severe category: ", "averageVotesReports":"Average votes per Report", "category2": "Most voted category: "]
    
    
    private func loadingDict(_ name:String)-> [String:String]{
        switch name{
        case "dashboardYourReports":
            return ["Reports Made": "...",
                    "Days Since Last Report": "...",
                    "Repaired(%)": "...",
                    "Average days to repair": "...",
                    "Average votes per Report":"..."]
            
        case "dashboardOverallReports":
            return ["Most severe category: ..." : "...",
                    "Average votes per repair": "...",
                    "Repaired(%)": "...",
                    "Average Days to Repair": "..."]
            
        case "dashboardYourVotes":
            return ["Voted Reports": "...",
                    "Repaired(%)": "...",
                    "Average Days to Repair":"...",
                    "Most voted category: ..." : "..."]
            
        default:
            return [:]
        }
    }
    
    
    
    func requestAll(){
        callFunction("dashboardYourReports", input: ["author" : self.uuid], identifier: self.uuid)
        callFunction("dashboardOverallReports", input: ["city" : city ], identifier: self.city)
        callFunction("dashboardYourVotes", input: ["user" : self.uuid], identifier: self.uuid)
        loadReports() // Este llamado solo deberia de realizarse al momento que se crea la vista.
    }
    
    func cityChanged(){
        callFunction("dashboardOverallReports", input: ["city" : city ], identifier: city)
    }
    
    func locationChanged(){
        loadReports()
    }
    
    
    
    private func callFunction(_ name:String, input: [String:Any], identifier: String){
        callFunctionLoading(name)
        functions.httpsCallable(name).call(input){result, error in
            if (error != nil){
                print("Error calling for points occurres, need to fix for EC")
                self.callFunctionFallback(name, identifier: identifier)
            }
            else if var data = result?.data as? [String: Any]{
                self.setDict(name, data: data)
                data["identifier"] = identifier
                self.defaults.set(data, forKey: name)
            }
        }
    }
    
    
    private func callFunctionLoading(_ name:String){
        setDictPrimitive(name, data: loadingDict(name))
    }
    
    
    private func callFunctionFallback(_ name:String, identifier:String){
        if var data = defaults.dictionary(forKey: name), let prevIdentifier = data["identifier"] as? String, prevIdentifier==identifier{
            data.removeValue(forKey: "identifier")
            setDict(name, data: data)
        }else{
            setDictPrimitive(name, data: fallbackDict(name))
        }
    }
    
    
    private func setDictPrimitive(_ name:String, data: [String:String]){
        
        switch name{
        case "dashboardYourReports":
            self.userReportsStats = data
            
        case "dashboardOverallReports":
            self.overallStats = data
            
        case "dashboardYourVotes":
            self.userVotesStats = data
            
        default:
            print("Rare case of dict parsing")
        }
    }
    
    
    private func setDict(_ name:String, data: [String:Any]){
        switch name{
        case "dashboardYourReports":
            self.userReportsStats = self.processStatsDict(data, name: name)
            
        case "dashboardOverallReports":
            self.overallStats = self.processStatsDict(data, name: name)
            
        case "dashboardYourVotes":
            self.userVotesStats = self.processStatsDict(data, name: name)
            
        default:
            print("Rare case of dict parsing")
        }
    }
    
    
    
    
    
    
    private func fallbackDict(_ name:String)-> [String:String]{
        
        switch name{
        case "dashboardYourReports":
            return ["Reports Made": "N/A",
                    "Days Since Last Report": "N/A",
                    "Repaired(%)": "N/A",
                    "Average Days to Repair": "N/A",
                    "Average votes per Report":"N/A"]
            
        case "dashboardOverallReports":
            return ["Most severe category: N/A" : "N/A",
                    "Average votes per repair": "N/A",
                    "Repaired(%)": "N/A",
                    "Average Days to Repair": "N/A"]
            
        case "dashboardYourVotes":
            return ["Voted Reports": "N/A",
                    "Repaired(%)": "N/A",
                    "Average Days to Repair":"N/A",
                    "Most voted category: N/A" : "N/A"]
            
        default:
            return [:]
        }
    }
    
    
    
    
    private func processStatsDict(_ dictToProcess:[String:Any], name: String) -> [String: String]{
        var dictProcessed: [String: String] = [:]
        var category: String? = nil
        var votes:Double? = -1
        for (key, value) in dictToProcess{
            if key == "category"{
                category = value as? String
            }else if key == "votes"{
                votes = value as? Double
            }else{
                dictProcessed[procesStatKey(key)] = processStatValue(value, key:key)
            }
        }
        if name == "dashboardOverallReports"{
            dictProcessed[procesStatKey("category1") + (category!.capitalized)] = processStatValue(votes as Any, key: "category")
        }else if name == "dashboardYourVotes"{
            dictProcessed[procesStatKey("category2") + (category!.capitalized)] = processStatValue(votes as Any, key: "category")
        }
        return dictProcessed
    }
    
    
    private func processStatValue(_ value: Any, key: String)-> String{
        if value is String{
            return (value as! String).capitalized
        }else if let numericValue = value as? Double{
            
            let correction = key == "repairPercentage" ? 100.0 : 1.0
            let finalValue = (numericValue < 0) ? "0" : String(format: "%.1f", numericValue * correction)
            return finalValue.replacingOccurrences(of: ".0", with: "")
        }else{
            return "N/A"
        }
        
    }
    
    
    private func procesStatKey(_ key: String) -> String {
        return DashboardViewModel.mapping[key] ?? "N/A"
    }
    
    private func loadReports(){
        db.collection("reports").whereField("author", isEqualTo: uuid).addSnapshotListener{(querySnapshot, error) in
            guard let documents = querySnapshot?.documents else{
                print("No documents")
                return
            }
            
            let reports = documents.compactMap{(queryDocumentSnapshot) -> Report? in
                return try? queryDocumentSnapshot.data(as: Report.self)}
            
            self.reports = []
            
            for report in reports{
                self.reports.append(ReportImage(report: report, image: UIImage(imageLiteralResourceName: "ImageLoading")))
            }
            
            self.loadImages()
            
        }
    }
    
    
    func loadImages(){

        DispatchQueue.global(qos: .utility).async {
            let storageManager = StorageManager()

            print("El tamaño del arreglo es", self.reports.count)
            for i in 0..<self.reports.count {
                print("Descargango imagen")
                
                if !self.networkMonitor.isConnected{
                    DispatchQueue.main.async {
                        self.reports[i].image = UIImage(imageLiteralResourceName: "ImageOfflineFallback")
                    }
                }else if let url = self.reports[i].report.media?.path {
                    storageManager.downloadImage(absolutePath: url)
                        .sink { [weak self] completion in
                            switch completion {
                            case.finished:
                                break
                            case.failure(let error):
                                DispatchQueue.main.async {
                                    self!.reports[i].image = UIImage(imageLiteralResourceName: "ImageOfflineFallback")
                                }
                                print("Error downloading image: \(error)")
                            }
                        } receiveValue: { [weak self] image in
                            DispatchQueue.main.async {
                                self!.reports[i].image = image
                            }
                        }
                        .store(in: &self.cancellables)
                }
            }
        }
    }
    
    
    func getDistanceKm(from: Location) -> String{
        if locationManager.region == nil{
            return "N/A"
        }
        
        let fromNew = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let toNew = CLLocation(latitude: locationManager.region!.center.latitude, longitude: locationManager.region!.center.longitude)
        let distance = toNew.distance(from: fromNew)/1000
        return String(format: "%.2f", distance) + " km"
    }
    
    
    
}

