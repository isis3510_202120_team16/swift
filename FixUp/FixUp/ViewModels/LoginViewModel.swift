//
//  LoginViewModel.swift
//  FixUp
//
//  Created by Camila Pantoja on 6/10/21.
//
import SwiftUI
import FirebaseAuth
import Firebase

class LoginViewModel: ObservableObject {
    let auth = Auth.auth()
    
    // SignIn status
    @Published var signedIn = false
    
    // Errors
    @Published var errorOcurred: Bool = false
    @Published var errorMessage: String = ""
    
    // Uploading status
    @Published var draftStatus = "A draft was detected. Continue where you left it!"
    
    // Controls if snackbar with status should be shown
    @Published var shouldPresentSnackbar = false
    
    // Save info from signup form initialized with default
    @Published var email: String = ""
    @Published var firstName: String = ""
    @Published var lastName: String = ""
    @Published var gender: String = "FemaleAvatar"
    
    // Key for signup draft
    private let SIGNUP_DRAFT_KEY = "SIGNUP_DRAFT_KEY";
    
    // User Defaults to use local storage
    let defaults = UserDefaults.standard

    // Network Monitor to check for internet connection, not subscribed
    let networkMonitor = NetworkMonitor.singleton
    
    
    var isSignedIn: Bool {
        return auth.currentUser != nil
    }
    
    func signIn(email: String, password: String) {
        auth.signIn(withEmail: email, password: password) { [weak self] result, error in
            guard result != nil, error == nil else {
                self?.errorMessage = error?.localizedDescription ?? "Unknown error ocurred"
                self?.errorOcurred = true
                return
            }
            DispatchQueue.main.async {
                // Success
                self?.errorOcurred = false
                self?.errorMessage = ""
                self?.signedIn = true
                
                Analytics.logEvent(AnalyticsEventLogin, parameters: nil)
            }
            //print(self?.auth.currentUser?.email ?? "No hay email")
            //print(self?.auth.currentUser?.displayName ?? "No hay display name")
            //print(self?.auth.currentUser?.photoURL ?? "No hay photo url")
        }
    }
    
    func signUp(password: String) {
        
        if !networkMonitor.isConnected{
            
            //If there is no connection a draft is saved with no password
            let draft = [
                "email": self.email,
                "firstName": self.firstName,
                "lastName": self.lastName,
                "gender": self.gender
            ]

            //print("Sign up sin conexión")
            //print(draft)
            
            self.defaults.set(draft, forKey: SIGNUP_DRAFT_KEY)
            
            self.errorOcurred = true
            
        }
        else{
            
            //Clear Userdefaults
            defaults.removeObject(forKey: SIGNUP_DRAFT_KEY)
            
            let displayName = "\(self.firstName) \(self.lastName)"
            
            //If there is connection a user is created with Firebase Auth
            auth.createUser(withEmail: self.email, password: password) { [weak self] result, error in
                guard result != nil, error == nil else {
                    self?.errorMessage = error?.localizedDescription ?? "Unknown error ocurred"
                    self?.errorOcurred = true
                    return
                }
                DispatchQueue.main.async {
                    // Success
                    self?.errorOcurred = false
                    self?.errorMessage = ""
                    self?.signedIn = true
                    
                    Analytics.logEvent(AnalyticsEventSignUp, parameters: nil)
                }
                
                let url = URL(string: self!.gender)
                
                let changeRequest = result?.user.createProfileChangeRequest()
                    changeRequest?.displayName = displayName
                    changeRequest?.photoURL = url
                    changeRequest?.commitChanges { error in
                        if let error = error {
                            print("An error occured while updating profile: \(error)")
                        } else {
                            print("Profile updated successfully while creating user")
                        }
                    }
            }
        }
        
    }
    
    func signOut() {
        try? auth.signOut()
        self.signedIn = false
    }
    
    func createError(message: String) {
        self.errorMessage = message
        self.errorOcurred = true
    }
    
    func loadSignUp() {
        // Attempt to retrieve a saved draft
        let draft = defaults.object(forKey: SIGNUP_DRAFT_KEY) as? [String:String]
        
        if let draft = draft {
            
            // If draft exists, initialize with saved draft
            
            self.email = draft["email"]!
            self.firstName = draft["firstName"]!
            self.lastName = draft["lastName"]!
            self.gender = draft["gender"]!
            
            self.shouldPresentSnackbar = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
                self.shouldPresentSnackbar = false
            }
            
            //Clear Userdefaults
            //defaults.removeObject(forKey: SIGNUP_DRAFT_KEY)
            
        }else {
            
            // If draft doesn't exist, initialize form with default values
            self.email = ""
            self.firstName = ""
            self.lastName = ""
            self.gender = "FemaleAvatar"
            
        }
    }
    
    
}
