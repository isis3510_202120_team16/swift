//
//  MapViewModel.swift
//  FixUp
//
//  Created by Juan David Serrano on 7/10/21.
//
import Foundation
import MapKit
import SwiftUI
import FirebaseFirestore
import FirebaseFirestoreSwift
import Firebase
import GoogleMaps

class MapViewModel: ObservableObject{
    
    @ObservedObject
    var locationManager:LocationManager = LocationManager.singleton
    
    @Published
    var reports: [Report] = []
    
    @Published
    var withFilter: Bool = false
    
    @Published
    var radius: Double = 200
    
    @ObservedObject
    var networkMonitor:NetworkMonitor = NetworkMonitor.singleton
    
    let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: GMSCameraPosition.camera(withLatitude: LocationManager.singleton.region?.center.latitude ?? MapDetails.defaultLocation.latitude,
                                                                                          longitude: LocationManager.singleton.region?.center.longitude ?? MapDetails.defaultLocation.longitude,
                                                                                          zoom: 15))
    var atLeastOnceCentered = false
    
    let maxTimeWriteFile :Double = 60*5
    
    let maxTimeLoadPoints :Double = 30
    
    var mapShowingSavedPoints = false
    
    let defaults = UserDefaults.standard
    
    let offset = 1.0
    
    static var maxRadius = 5000.0
    
    private let db = Firestore.firestore()
    
    lazy var functions = Functions.functions()
    
    var lastMapShown: [String: Double]? = nil
    
    private func stillInsideVecinity(_ vecinityMetaData: Dictionary<String, Double>, _ userLocation: CLLocationCoordinate2D) -> Bool{
        if vecinityMetaData["userLatitude"] != nil && vecinityMetaData["userLongitude"] != nil{
            let centerOldVecinity = CLLocation(latitude: vecinityMetaData["userLatitude"]!, longitude: vecinityMetaData["userLongitude"]!)
            let userLocationHelper = CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)

            return centerOldVecinity.distance(from: userLocationHelper) <= MapViewModel.maxRadius
        }
        return false
    }
    
    
    private func mapInsideVecinity(_ vm: Dictionary<String, Double>?, _ visibleRegion: GMSVisibleRegion) -> Bool{
        
        let dM = getDictVisibleRegion(visibleRegion)
        
        if vm != nil{
            return vm!["southwestLatitude"]! ... vm!["northeastLatitude"]! ~= dM["northeastLatitude"]! && vm!["southwestLatitude"]! ... vm!["northeastLatitude"]! ~= dM["southwestLatitude"]! && vm!["southwestLongitude"]! ... vm!["northeastLongitude"]! ~= dM["southwestLongitude"]! && vm!["southwestLongitude"]! ... vm!["northeastLongitude"]! ~= dM["northeastLongitude"]!
        }else{
            return false
        }
    }
    
    private func getDictVisibleRegion(_ gmsVisibleRegion: GMSVisibleRegion ) -> Dictionary<String, Double>{
        var dictVisibleRegion =  ["northeastLatitude": [gmsVisibleRegion.nearLeft.latitude, gmsVisibleRegion.nearRight.latitude, gmsVisibleRegion.farLeft.latitude, gmsVisibleRegion.farLeft.latitude].max()!,
                                  "northeastLongitude": [gmsVisibleRegion.nearLeft.longitude, gmsVisibleRegion.nearRight.longitude, gmsVisibleRegion.farLeft.longitude, gmsVisibleRegion.farLeft.longitude].max()!,
                                  "southwestLatitude": [gmsVisibleRegion.nearLeft.latitude, gmsVisibleRegion.nearRight.latitude, gmsVisibleRegion.farLeft.latitude, gmsVisibleRegion.farLeft.latitude].min()!,
                                  "southwestLongitude": [gmsVisibleRegion.nearLeft.longitude, gmsVisibleRegion.nearRight.longitude, gmsVisibleRegion.farLeft.longitude, gmsVisibleRegion.farLeft.longitude].min()!]
        
        let diffLat =  dictVisibleRegion["northeastLatitude"]! - dictVisibleRegion["southwestLatitude"]!
        let diffLong = dictVisibleRegion["northeastLongitude"]! - dictVisibleRegion["southwestLongitude"]!
        
        dictVisibleRegion["northeastLatitude"]  =  dictVisibleRegion["northeastLatitude"]! + offset/2 * diffLat
        dictVisibleRegion["northeastLongitude"]  =  dictVisibleRegion["northeastLongitude"]! + offset/2 * diffLong
        dictVisibleRegion["southwestLatitude"]  =  dictVisibleRegion["southwestLatitude"]! - offset/2 * diffLat
        dictVisibleRegion["southwestLongitude"]  =  dictVisibleRegion["southwestLongitude"]! - offset/2 * diffLong
                                  
        return dictVisibleRegion
    }
    
    
    public func  getReports(mapMoved: Bool){
        
        let userLocation = locationManager.region?.center ?? MapDetails.defaultLocation
        
        if !networkMonitor.isConnected && withFilter{
            withFilter.toggle()
            //lostConnectionError = true
        }else{
            refreshSavedPoints(userLocation)
            updateMapPoints(mapMoved, userLocation)
        }
        
        
        
        
    }
    
    
    private func refreshSavedPoints(_ userLocation: CLLocationCoordinate2D){
        var input = ["distance":MapViewModel.maxRadius, "userLatitude": userLocation.latitude, "userLongitude": userLocation.longitude]
        if networkMonitor.isConnected{
            if let vecinityMetaData = defaults.dictionary(forKey: "vecinityMetaData") as? [String:Double]{
                if !stillInsideVecinity(vecinityMetaData, userLocation) || NSDate().timeIntervalSince1970 - (vecinityMetaData["ts"] ?? 0.0 ) >  maxTimeWriteFile {
                    functions.httpsCallable("filterPointsDistance").call(input){result, error in
                        if (error != nil){
                            print("Error calling for points occurres, need to fix for EC")
                        }
                        
                        else if let data = result?.data as? [String: Any], let newPoints = data["filteredPoints"] as? [Any]{
                            var newReports: [Report] = [].self
                            for newPoint in newPoints{
                                newReports.append(try! Firestore.Decoder().decode(Report.self, from: newPoint as! [String:Any]))
                            }
                            if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
                                let filename = documentsDirectory.appendingPathComponent("vecinityReports.json")
                                do{
                                    let encoder = JSONEncoder()
                                    encoder.outputFormatting = .prettyPrinted
                                    let data = try! encoder.encode(newReports)
                                    let dataString = String(data: data, encoding: .utf8)!
                                    try dataString.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
                                    input["ts"] = NSDate().timeIntervalSince1970
                                    self.defaults.set(input, forKey: "vecinityMetaData")
                                }catch{
                                    print("No se pudo escribir al archivo")
                                }
                            }
                        }
                    }
                }else{
                    // No hago nada ya estaban guardados unos puntos y no han expirado
                }
            }else{
                // Nunca se han guardado entonces me toca
                functions.httpsCallable("filterPointsDistance").call(input){result, error in
                    if (error != nil){
                        print("Error calling for points occurres, need to fix for EC")
                    }
                    
                    else if let data = result?.data as? [String: Any], let newPoints = data["filteredPoints"] as? [Any]{
                        var newReports: [Report] = [].self
                        for newPoint in newPoints{
                            newReports.append(try! Firestore.Decoder().decode(Report.self, from: newPoint as! [String:Any]))
                        }
                        if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
                            let filename = documentsDirectory.appendingPathComponent("vecinityReports.json")
                            do{
                                let encoder = JSONEncoder()
                                encoder.outputFormatting = .prettyPrinted
                                let data = try! encoder.encode(newReports)
                                let dataString = String(data: data, encoding: .utf8)!
                                try dataString.write(to: filename, atomically: true, encoding: String.Encoding.utf8)
                                input["ts"] = NSDate().timeIntervalSince1970
                                self.defaults.set(input, forKey: "vecinityMetaData")
                            }catch{
                                print("No se pudo escribir al archivo")
                            }
                        }
                    }
                }
            }
        }else{
            // No hay internet no hay caso, tengo que dejar lo que este si es que existe
        }
    }
    
    
    private func updateMapPoints(_ mapMoved: Bool, _ userLocation: CLLocationCoordinate2D){
        
        let visibleRegion = mapView.projection.visibleRegion()
        
        if withFilter && !mapMoved{
            let input = ["distance":radius, "userLatitude": userLocation.latitude, "userLongitude": userLocation.longitude]
            
            functions.httpsCallable("smartFeature").call(input){result, error in
                if (error != nil){
                    print("Error while fetching reports from filter")
                    
                }
                else if let data = result?.data as? [String: Any], let newPoints = data["filteredPoints"] as? [Any]{
                    var newReports: [Report] = [].self
                    for newPoint in newPoints{
                        newReports.append(try! Firestore.Decoder().decode(Report.self, from: newPoint as! [String:Any]))
                    }
                    self.reports = newReports
                    self.mapShowingSavedPoints = false
                    self.lastMapShown = nil
                }
            }
        }else if !withFilter{
            if !networkMonitor.isConnected{
                // Caso de carga de archivo para msotrar puntos
                if let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first, !mapShowingSavedPoints{
                    let filename = documentsDirectory.appendingPathComponent("vecinityReports.json")
                    
                    do{
                        let decoder = JSONDecoder()
                        let dataString = try String(contentsOf: filename, encoding: String.Encoding.utf8)
                        
                        
                        self.reports = try decoder.decode([Report].self, from: dataString.data(using: .utf8)! )
                        self.mapShowingSavedPoints = true
                        self.lastMapShown = nil
                    }catch{
                        // Error que no podriamos solucionar
                        print("No se pudo leer el archivo")
                    }
                }
            }else{
                if lastMapShown != nil {
                    let timeout = NSDate().timeIntervalSince1970 - (lastMapShown?["ts"] ?? 0.0 ) >  maxTimeLoadPoints
                    let userInMap = stillInsideVecinity(lastMapShown!, userLocation)
                    let mapInsidePointsShown = mapInsideVecinity(lastMapShown!, visibleRegion)
                    if timeout || !userInMap || !mapInsidePointsShown{
                        var input = self.getDictVisibleRegion(visibleRegion)
                        self.functions.httpsCallable("filterPoints").call(input){result, error in
                            if (error != nil){
                                print("Error calling for points occurres, need to fix for EC")
                            }
                            else if let data = result?.data as? [String: Any], let newPoints = data["filteredPoints"] as? [Any]{
                                var newReports:[Report] = []
                                var newPoint: [String:Any]? = nil
                                var newReport: Report? = nil
                                for i in 0..<newPoints.count {
                                    newPoint = newPoints[i] as? [String:Any]
                                    newReport = try? Firestore.Decoder().decode(Report.self, from: newPoint!)
                                    if newReport != nil{
                                        newReports.append(newReport!)
                                    }
                                }
                                self.reports = newReports
                                self.mapShowingSavedPoints = false
                                input["ts"] = NSDate().timeIntervalSince1970
                                self.lastMapShown = input
                            }
                        }
                    }
                }else{
                    var input = self.getDictVisibleRegion(visibleRegion)
                    self.functions.httpsCallable("filterPoints").call(input){result, error in
                        if (error != nil){
                            // No recovery error
                            print("Error calling for points occurres, need to fix for EC")
                        }
                        else if let data = result?.data as? [String: Any], let newPoints = data["filteredPoints"] as? [Any]{
                            var newReports:[Report] = []
                            var newPoint: [String:Any]? = nil
                            var newReport: Report? = nil
                            for i in 0..<newPoints.count {
                                newPoint = newPoints[i] as? [String:Any]
                                newReport = try? Firestore.Decoder().decode(Report.self, from: newPoint!)
                                if newReport != nil{
                                    newReports.append(newReport!)
                                }
                            }
                            self.reports = newReports
                            self.mapShowingSavedPoints = false
                            input["ts"] = NSDate().timeIntervalSince1970
                            self.lastMapShown = input
                        }
                    }
                }
            }
        }else{
            // Caso adicional no se necesita porque a fin de cuentas si mueve el mapa estando en el filtro no se deberia afectar el filtro y no se deben realizar cargas innecesarias
        }
        
        
        
    }
    
    private func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> Double{
        let fromNew = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let toNew = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return toNew.distance(from: fromNew)
    }
    
    
    func getReportMarkerColor(_ category: ReportCategory) -> Color{
        switch category {
        case .Road:
            return Color("RoadColor")
        case .Sidewalk:
            return Color("SidewalkColor")
        case .Vandalism:
            return Color("VandalismColor")
        };
    }
    
}
