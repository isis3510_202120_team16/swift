//
//  ReportDetailViewModel.swift
//  FixUp
//
//  Created by Camila Pantoja on 11/11/21.
//

import Foundation
import Combine
import CoreLocation
import SwiftUI
import FirebaseAuth
import FirebaseFirestore

final class ReportDetailViewModel: ObservableObject {
    var reportRepository = ReportRepository()
    var usersRepository = UsersRepository()
    
    @Published var report: Report
    @Published var downloadedMedia: UIImage
    @Published var hasMedia: Bool = false
    @Published var userHasVoted: Bool
    private var networkMonitor = NetworkMonitor.singleton
    private let db = Firestore.firestore()
    
    //    var id = ""
    
    var uid = Auth.auth().currentUser!.uid
    
    private var cancellables: Set<AnyCancellable> = []
    
    init(report: Report) {
        // Cached report
        self.report = report
        print("Cached report: \(report)")
        self.userHasVoted = true
        self.downloadedMedia = UIImage(imageLiteralResourceName: "ImageLoading")
        self.hasMedia = false
        
        if networkMonitor.isConnected {
            getReport()
        } else {
            self.downloadMedia()
        }
    }
    
    func getReport() {
        guard let reportId = report.id else { return }
        guard networkMonitor.isConnected else {
            self.downloadedMedia = UIImage(imageLiteralResourceName: "ImageOfflineFallback")
            self.hasMedia = false
            return
        }
        db.collection("reports").document(reportId).addSnapshotListener{(documentSnapshot, error) in
            guard let document = documentSnapshot else {
                print("No document found!!!")
                self.downloadMedia()
                self.userHasVoted = self.getIfUserHasVoted()
                return
            }
            do {
                guard let fetchedReport = try document.data(as: Report.self) else {
                    return
                }
                self.report = fetchedReport
                self.report.id = reportId
            } catch {
                print("Error: \(error)")
            }
            self.downloadMedia()
            self.userHasVoted = self.getIfUserHasVoted()
        }
    }
    
    func getDistance(from: CLLocation) -> Double {
        let coordinate1 = CLLocation(latitude: report.damageLocation.latitude, longitude: report.damageLocation.longitude)
        return from.distance(from: coordinate1)
    }
    
    func downloadMedia() {
        guard let path = report.media?.path else { return }
        
        // Search for image in cache
        let cachedImage = ImageCacheManager.instance.get(key: path)
        if let unwrappedImage = cachedImage {
            self.downloadedMedia = unwrappedImage
            self.hasMedia = true
            return
        }
        
        // Check for internet connection
        guard networkMonitor.isConnected else {
            self.downloadedMedia = UIImage(imageLiteralResourceName: "ImageOfflineFallback")
            self.hasMedia = false
            return
        }
        
        let storageManager = StorageManager()
        
        DispatchQueue.global(qos: .utility).async {
            storageManager.downloadImage(absolutePath: path)
                .sink { [weak self] completion in
                    switch completion {
                    case.finished:
                        break
                    case.failure(let error):
                        DispatchQueue.main.async {
                            self?.downloadedMedia = UIImage(imageLiteralResourceName: "ImageOfflineFallback")
                            self?.hasMedia = false
                        }
                        print("Error downloading image: \(error)")
                    }
                } receiveValue: { [weak self] image in
                    ImageCacheManager.instance.add(key: path, value: image);
                    DispatchQueue.main.async {
                        self?.downloadedMedia = image
                        self?.hasMedia = true
                    }
                }
                .store(in: &self.cancellables)
        }
        
        
    }
    
    func addVoteReport(report: Report ) {
        guard networkMonitor.isConnected else { return }
        // Add vote to new user in collection users using repository
        self.reportRepository.addVoteUser(report: report)
        self.report.numVotes = (self.report.numVotes ?? 0) + 1
    }
    
    func addVoteNewUser(usersVotes: UsersVotes ) {
        // Add vote to new user in collection users using repository
        do {
            try self.usersRepository.add(usersVote: usersVotes)
            self.userHasVoted = true
        } catch {
            print("Error creating new UsersVotes in db")
        }
        return
    }
    
    func getIfUserHasVoted() -> Bool {
        guard networkMonitor.isConnected else {
            print("Disabling voting because of lack of connectivity")
            return true
        }
        
        let userID = uid
        //var usersVotes = reportDetailViewModel.usersRepository.$usersVotes
        var boolResp = false
        
        for user in usersRepository.usersVotes {
            if user.id == userID {
                for reportVoted in user.votedReports {
                    if(reportVoted == report.id) {
                        boolResp = true
                    }
                }
            }
        }
        
        return boolResp
    }
    
    func addVoteExistingUser(pUserId: String, pReportId: String) {
        guard networkMonitor.isConnected else { return }
        
        // Add vote to new user in collection users using repository
        do {
            try self.usersRepository.update(userId: pUserId, reportId: pReportId)
            self.userHasVoted = true
        } catch {
            print("Error creating new UsersVotes in db")
        }
        return
    }
    
}
