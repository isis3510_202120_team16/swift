//
//  SignUpViewModel.swift
//  FixUp
//
//  Created by Daniela González on 2/12/21.
//

import SwiftUI
import FirebaseAuth

class SignUpViewModel: ObservableObject {
    let auth = Auth.auth()

    @Published var errorOcurred: Bool = false
    @Published var errorMessage: String = ""
    
    func signUp(email: String, password: String) {
        auth.signIn(withEmail: email, password: password) { [weak self] result, error in
            guard result != nil, error == nil else {
                self?.errorMessage = error?.localizedDescription ?? "Unknown error ocurred"
                self?.errorOcurred = true
                return
            }
            DispatchQueue.main.async {
                // Success
                self?.errorOcurred = false
                self?.errorMessage = ""
            }
        }
    }
    
    func signUp(email: String, password: String) {
        auth.createUser(withEmail: email, password: password) { [weak self] result, error in
            guard result != nil, error == nil else {
                self?.errorMessage = error?.localizedDescription ?? "Unknown error ocurred"
                self?.errorOcurred = true
                return
            }
            DispatchQueue.main.async {
                // Success
                self?.errorOcurred = false
                self?.errorMessage = ""
                self?.signedIn = true
            }
        }
    }
    
    func signOut() {
        try? auth.signOut()
        self.signedIn = false
    }
    
}

