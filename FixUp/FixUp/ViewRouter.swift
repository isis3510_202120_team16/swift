//
//  ViewRouter.swift
//  FixUp
//
//  Created by Camila Pantoja on 6/10/21.
//

import SwiftUI

class ViewRouter: ObservableObject {
    @Published var currentItem: TabBarViewModel = .map
    
    var view: some View { return currentItem.view }
}

enum TabBarViewModel: Int, CaseIterable {
    case profile
    case dashboard
    case map
    case notifications
    
    var imageName: String {
        switch self {
        case .profile:
            return "person.fill"
        case .dashboard:
            return "chart.bar.fill"
        case .map:
            return "mappin.and.ellipse"
        case .notifications:
            return "bell.fill"
        }
    }
    
    var analyticsEventName: String {
        switch self {
        case .profile:
            return AnalyticsEventScreenProfile
        case .dashboard:
            return AnalyticsEventScreenDashboard
        case .map:
            return AnalyticsEventScreenMap
        case .notifications:
            return AnalyticsEventScreenNotifications
        }
    }
    
    @ViewBuilder
    var view: some View {
        switch self {
        case .profile: ProfileView()
        case .dashboard: DashboardView()
        case .map: GoogleMapView()
        case .notifications: NotificationsView()
        }
    }
}
