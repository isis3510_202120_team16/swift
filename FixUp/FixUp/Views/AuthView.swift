//
//  AuthView.swift
//  FixUp
//
//  Created by Daniela González on 2/12/21.
//

import SwiftUI

struct AuthView: View {
    
    @State var showLoginForm = true
    
    @State var firstName = ""
    @State var lastName = ""
    @State var gender = "FemaleAvatar"
    @State var email = ""
    @State var password = ""
    @State var emailSignUp = ""
    @State var passwordSignUp = ""
    @State var repeatedPassword = ""
    
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    @ObservedObject var monitor = NetworkMonitor.singleton
    
    
    var body: some View {
        
        NavigationView {
            VStack{
                
                //LOGIN VIEW
                if self.showLoginForm{
                    VStack {
                        Image("FixUpLogo")
                            .padding(35)
                            .padding(.top, 40)
                        
                        Group {
                            Text("LOGIN")
                                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color("PrimaryColor"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.horizontal)
                                .padding(.top, 20)
                            
                            Text("Email")
                                .foregroundColor(Color(.secondaryLabel))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding()
                            
                            TextField("", text: $email)
                                .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                .padding(.horizontal)
                                .autocapitalization(UITextAutocapitalizationType.none)
                                .disableAutocorrection(true)
                            
                            Text("Password")
                                .foregroundColor(Color(.secondaryLabel))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding()
                            
                            SecureField("", text: $password)
                                .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                .padding(.horizontal)
                            
                            Text("Forgot password?")
                                .font(.caption)
                                .foregroundColor(Color(.tertiaryLabel))
                                .frame(maxWidth: .infinity, alignment: .trailing)
                                .padding(.horizontal, 35)
                        }
                        .alert(isPresented: $loginViewModel.errorOcurred,
                               content: {
                                
                                if(!monitor.isConnected){
                                    return Alert(title: Text("No Connection"), message: Text("You are offline. Please check your internet connection and try later."))
                                }
                                else{
                                    return Alert(title: Text("Error"), message: Text(loginViewModel.errorMessage))
                                }
                               })
                        /*.alert(isPresented: $showNotConnectionAlert,
                               content: {
                                return Alert(title: Text("Error"), message: Text("No Connection"))
                               })
                        */
                        Button(action: {
                            guard !email.isEmpty, !password.isEmpty else {
                                loginViewModel.createError(message: "All fields must be filled in to sign up!")
                                return
                            }
                            /*if !monitor.isConnected{
                                showNotConnectionAlert = true
                            }*/
                            loginViewModel.signIn(email: email, password: password)
                        }) {
                            Text("LOGIN")
                                .foregroundColor(.white)
                                .font(.system(size: 20, weight: .medium))
                                .padding(.vertical, 10)
                                .frame(maxWidth: .infinity)
                                .background(Color("PrimaryColor"))
                                .cornerRadius(8)
                                .padding()
                        }
                        
                        Spacer()
                        
                        HStack {
                            Spacer()
                            Text("Don't have an account?")
                                .foregroundColor(Color(.secondaryLabel))
                                //.frame(maxWidth: .infinity, alignment: .center)
                                .padding(.vertical, 10)
                            Button(action: { loginViewModel.loadSignUp(); self.showLoginForm.toggle() }) {
                                Text("Sign Up")
                                    .foregroundColor(Color("VandalismColor"))
                                    .underline()
                            }
                            Spacer()
                        }
                        .padding(.bottom, 20)
                        
                    }
                    .background(LoginBackground())
                    .edgesIgnoringSafeArea(.all)
                }
                
                //SIGN UP VIEW
                else {
                    ScrollView{
                    VStack {
                        Group {
                            Text("SIGN UP")
                                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                                .foregroundColor(Color("PrimaryColor"))
                                .frame(maxWidth: .infinity, alignment: .leading)
                                .padding(.top, 90)
                                .padding(.bottom, 10)
                                .padding(.horizontal)
                            
                            Group {
                                Text("First Name")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                TextField("", text: $loginViewModel.firstName)
                                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                    .padding(.horizontal)
                                    .autocapitalization(UITextAutocapitalizationType.none)
                                    .disableAutocorrection(true)
                            }
                            
                            Group {
                                Text("Last Name")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                TextField("", text: $loginViewModel.lastName)
                                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                    .padding(.horizontal)
                                    .autocapitalization(UITextAutocapitalizationType.none)
                                    .disableAutocorrection(true)
                            }
                            
                            Group {
                                Text("Gender")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                Picker("Gender", selection: $loginViewModel.gender) {
                                        Text("Female").tag("FemaleAvatar").foregroundColor(.white)
                                        Text("Male").tag("MaleAvatar").foregroundColor(.white)
                                }
                                //.pickerStyle(.radioGroup)
                                //.horizontalRadioGroupLayout()
                                .pickerStyle(SegmentedPickerStyle())
                                .padding(.horizontal)
                                .colorMultiply(Color(red: 0.85, green: 0.722, blue: 0.69))
                            }
                            
                            Group {
                                Text("Email")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                TextField("", text: $loginViewModel.email)
                                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                    .padding(.horizontal)
                                    .autocapitalization(UITextAutocapitalizationType.none)
                                    .disableAutocorrection(true)
                            }
                            
                            Group {
                                Text("Password")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                SecureField("", text: $passwordSignUp)
                                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                    .padding(.horizontal)
                            }
                            
                            Group {
                                Text("Repeat Password")
                                    .foregroundColor(Color(.secondaryLabel))
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .padding(.top)
                                    .padding(.horizontal)
                                
                                SecureField("", text: $repeatedPassword)
                                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                                    .padding(.horizontal)
                            }
                            
                        }
                        .alert(isPresented: $loginViewModel.errorOcurred,
                               content: {
                                
                                if(!monitor.isConnected){
                                    return Alert(title: Text("No Connection"), message: Text("You are offline. Please check your internet connection and try later."))
                                }
                                else{
                                    return Alert(title: Text("Error"), message: Text(loginViewModel.errorMessage))
                                }
                               })
                        /*.alert(isPresented: $showNotConnectionAlert,
                               content: {
                                return Alert(title: Text("Error"), message: Text("No Connection"))
                               })
                        */
                        Button(action: {
                            /*guard !firstName.isEmpty, !lastName.isEmpty, !emailSignUp.isEmpty, !passwordSignUp.isEmpty, !repeatedPassword.isEmpty else */
                            
                            guard !loginViewModel.firstName.isEmpty, !loginViewModel.lastName.isEmpty, !loginViewModel.email.isEmpty, !passwordSignUp.isEmpty, !repeatedPassword.isEmpty else {
                                
                                loginViewModel.createError(message: "All fields must be filled in to sign up!")
                                return
                            }
                            /*if !monitor.isConnected{
                                showNotConnectionAlert = true
                            }*/
                            
                            if passwordSignUp != repeatedPassword {
                                loginViewModel.createError(message: "Passwords do not match. Please try again!")
                            }
                            else {
                                loginViewModel.signUp(password: passwordSignUp)
                            }
                            
                        }) {
                            Text("SIGN UP")
                                .foregroundColor(.white)
                                .font(.system(size: 20, weight: .medium))
                                .padding(.vertical, 10)
                                .frame(maxWidth: .infinity)
                                .background(Color("PrimaryColor"))
                                .cornerRadius(8)
                                .padding()
                        }
                        .padding(.top, 20)
                        
                        HStack {
                            Spacer()
                            Text("Already have an account?")
                                .foregroundColor(Color(.secondaryLabel))
                                //.frame(maxWidth: .infinity, alignment: .center)
                            Button(action: { self.showLoginForm.toggle() }) {
                                Text("Login")
                                    .foregroundColor(Color("VandalismColor"))
                                    .underline()
                            }
                            Spacer()
                        }
                        .padding(.top, 45)
                        //.padding(.bottom, 20)
                         
                    }
                    .edgesIgnoringSafeArea(.all)
                    .padding(.bottom, 20)
                        //.background(SignUpBackground())
                    Spacer()
                    }
                    .background(SignUpBackground())
                    .navigationBarHidden(true)
                    .edgesIgnoringSafeArea(.all)
                    .popup(isPresented: loginViewModel.shouldPresentSnackbar, alignment: .top, direction: .top, content: { Snackbar(label: loginViewModel.draftStatus) })
                }
            }
            
        }
        
        
    }
}

struct AuthView_Previews: PreviewProvider {
    static var previews: some View {
        AuthView()
    }
}
