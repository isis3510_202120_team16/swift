//
//  CreateReportView.swift
//  FixUp
//
//  Created by Daniela González on 5/10/21.
//

import SwiftUI
import MapKit

class NavRouteModel: ObservableObject {
    @Published var currentTag: Int?
}

struct CreateReportView: View {
    
    // Location Manager for current location
    @StateObject private var locationManager = LocationManager.singleton
    
    // ViewModel for report creation
    @ObservedObject var viewModel: CreateReportViewModel
    
    // Binding with parent view to hide or show modal
    @Binding var showModal: Bool
    
    // Look at SUImagePickerView for details
    // @State private var image: Image? = Image(systemName: "plus") --> This is now a published variable in view model
    // @State private var uiImage: UIImage? --> This is now a published variable in view model
    @State private var shouldPresentImagePicker = false
    @State private var shouldPresentActionScheet = false
    @State private var shouldPresentCamera = false
    
    @StateObject private var vm = NavRouteModel()
    
    init(showModal: Binding<Bool>) {
        self._showModal = showModal
        let userLocation = LocationManager.singleton.region?.center ?? MapDetails.defaultLocation
        viewModel = CreateReportViewModel(initLat: userLocation.latitude, initLong: userLocation.longitude)
    }
    
    /**
     CAREFUL! THIS IS RECURSIVE
     THIS IS CALLED AFTER TRYING TO CREATE REPORT ONLY (checks for complete in order to close modal)
     */
    func checkToCloseModal() {
        // Do after 4 seconds in main
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            if (viewModel.uploadStatus == UploadStatus.isUploading) {
                // Report is still uploading, be patient, try again in three seconds (recursion)
                checkToCloseModal()
            } else if (viewModel.uploadStatus == UploadStatus.completed) {
                // Report was updated successfully, close modal, close snackbar, restart status
                showModal = false
                viewModel.shouldPresentSnackbar = false
                viewModel.uploadStatus = UploadStatus.notStarted
            } else {
                // Some other thing ocurred :( snackbar can be dismissed
                viewModel.shouldPresentSnackbar = false
            }
        }
    }
    
    var body: some View {
        
        NavigationView {
            Form{
                Section{
                    NavigationLink(destination: SelectLocationView(chosenLat: $viewModel.report.damageLocation.latitude, chosenLong: $viewModel.report.damageLocation.longitude, reportLocation: MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: LocationManager.singleton.region?.center.latitude ?? MapDetails.defaultLocation.latitude, longitude: LocationManager.singleton.region?.center.longitude ?? MapDetails.defaultLocation.longitude), span: MapDetails.defaultSpan))){
                        HStack{
                            Image(systemName: "location.fill")
                                .resizable()
                                .foregroundColor(Color("PrimaryColor"))
                                .frame(width: 25, height: 25, alignment: .center)
                                .padding([.top, .bottom, .trailing])
                            VStack{
                                HStack{
                                    Text("Location")
                                    Spacer()
                                }
                                HStack{
                                    Text("\(viewModel.report.damageLocation.latitude), \(viewModel.report.damageLocation.longitude)")
                                        .foregroundColor(Color.gray)
                                    Spacer()
                                }
                            }
                            .padding(.leading, 4)
                            
                        }
                    }
                }
                Section{
                    Text("Severity")
                    GeometryReader { geometry in
                        RatingView(rating: $viewModel.report.severity)
                            .frame(width: geometry.size.width, height: geometry.size.height, alignment: .center)
                    }
                    .padding(.vertical, 25)
                }
                
                Section{
                    Picker("Type", selection: $viewModel.report.type ?? ReportType.Damage) {
                        ForEach(ReportType.allCases, id:\.self){
                            Text("\($0.rawValue)")
                        }
                    }
                }
                Section {
                    Picker("Category", selection: $viewModel.report.category) {
                        ForEach(ReportCategory.allCases, id:\.self){
                            Text("\($0.rawValue)")
                        }
                    }
                    //.pickerStyle(WheelPickerStyle())
                }
                //Section(header: Text("DESCRIPTION"))
                Section{
                    Text("Description")
                    /*TextField(
                     "Write a descriptin about the damage",
                     text: $description
                     )
                     .lineLimit(4)*/
                    TextEditor(text: $viewModel.report.description ?? "")
                }
                
                Section {
                    Text("Add photo to report")
                    ZStack{
                        Rectangle()
                            .fill(Color("Gray"))
                        viewModel.image!
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .clipShape(Rectangle())
                            .frame(width: 45, height: 45)
                            .foregroundColor(Color("PrimaryColor"))
                            .padding()
                            .onTapGesture { self.shouldPresentActionScheet = true }
                            .sheet(isPresented: $shouldPresentImagePicker) {
                                SUImagePickerView(sourceType: self.shouldPresentCamera ? .camera : .photoLibrary, image: $viewModel.image, uiImage: $viewModel.uiImage, isPresented: self.$shouldPresentImagePicker)
                                    .edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
                            }.actionSheet(isPresented: $shouldPresentActionScheet) { () -> ActionSheet in
                                ActionSheet(title: Text("Choose mode"), message: Text("Please choose your preferred mode to select an image"), buttons: [ActionSheet.Button.default(Text("Camera"), action: {
                                    self.shouldPresentImagePicker = true
                                    self.shouldPresentCamera = true
                                }), ActionSheet.Button.default(Text("Photo Library"), action: {
                                    self.shouldPresentImagePicker = true
                                    self.shouldPresentCamera = false
                                }), ActionSheet.Button.cancel()])
                            }
                    }
                    .frame(width: 100, height: 100)
                    .padding()
                }
                
                Section{
                    HStack {
                        Spacer()
                        
                        Button(action: {
                            let createdLocation = Location(latitude: LocationManager.singleton.region?.center.latitude ?? MapDetails.defaultLocation.latitude, longitude: LocationManager.singleton.region?.center.longitude ?? MapDetails.defaultLocation.longitude)
                            guard viewModel.uiImage != nil else { return }
                            viewModel.addReport(createdLocation: createdLocation)
                            viewModel.shouldPresentSnackbar = true
                            checkToCloseModal()
                        }){
                            Text("CREATE REPORT")
                                .fontWeight(.semibold)
                                .font(.system(size: 18))
                                .tracking(3)
                                .padding(.vertical, 10)
                                .background( viewModel.uiImage != nil ? Color("PrimaryColor") : Color.gray)
                                .foregroundColor(.white)
                                .cornerRadius(10)
                        }.disabled(!(viewModel.uiImage != nil))
                        Spacer()
                    }
                    /*.background(Color(red: 0.9882, green: 0.4, blue: 0.26275).ignoresSafeArea(edges: .all))
                     .cornerRadius(10)
                     .edgesIgnoringSafeArea(.all)*/
                }.listRowBackground(viewModel.uiImage != nil ? Color("PrimaryColor") : Color.gray)
                
                
            }
            .navigationTitle("Create a new report")
        }
        .background(
            ZStack(alignment: .bottom){
                RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                Rectangle()
                    .frame(height: UIScreen.main.bounds.height * 0.93 / 2)
            }
            .foregroundColor(.white)
            .frame(alignment: .bottom)
        )
        .transition(.move(edge: .bottom))
        .onAppear {
            viewModel.initialTime = Date()
            // Make sure user knows his draft is there, be it from initializing view or not
            if (viewModel.uploadStatus == UploadStatus.connectionError) {
                viewModel.uploadStatus = UploadStatus.pendingDraft
                viewModel.shouldPresentSnackbar = true
            }
        }
        .popup(isPresented: viewModel.shouldPresentSnackbar, alignment: .top, direction: .top, content: { Snackbar(label: viewModel.uploadStatus.rawValue) })
        
        
    }
}

struct CreateReportView_Previews: PreviewProvider {
    static var previews: some View {
        CreateReportView(showModal: .constant(true))
    }
}

struct Snackbar: View {
    var label: String
    
    var body: some View {
        Text(label)
            .foregroundColor(.white)
            .font(.system(size: 20, weight: .medium))
            .padding(.vertical, 10)
            .frame(maxWidth: .infinity)
            .background( Color.gray )
            .cornerRadius(8)
            .padding()
    }
}
