//
//  DashboardView.swift
//  FixUp
//
//  Created by Juan David Serrano on 5/10/21.
//

import SwiftUI




struct DashboardView: View {
    
    @ObservedObject
    var viewModel = DashboardViewModel()
    
    @ObservedObject
    var networkMonitor = NetworkMonitor.singleton
    
    @ObservedObject
    var locationManager = LocationManager.singleton
    
    func makeReportItemBinding(_ item: ReportImage) -> Binding<ReportImage>? {
        guard let index = viewModel.reports.firstIndex(where: { $0.id == item.id }) else { return nil }
        return Binding.init(get: { self.viewModel.reports[index] },
                            set: { self.viewModel.reports[index] = $0 })
    }
    
    var body: some View {
        VStack{
            HStack{
                Text("Dashboard").font(.title).bold().padding([.top, .leading])
                Spacer()
                Button("Refresh") {
                    viewModel.requestAll()
                }.padding(.trailing).foregroundColor(networkMonitor.isConnected ? Color("PrimaryColor") : Color(red: 0.988, green: 0.4, blue: 0.263, opacity: 0.5)).disabled(!networkMonitor.isConnected)
            }
            if !networkMonitor.isConnected{
                
                Snackbar(label: "No internet, showing previously saved data. N/A if no data available.")
                
            }
            ScrollView{
                VStack{
                    ScrollView(.horizontal){
                        HStack{
                            ForEach(viewModel.reports, id: \.self) { report in
                                if let bindedReport = makeReportItemBinding(report) {
                                    DashboardReport(reportImage: bindedReport, viewModel: viewModel)
                                        .padding(.all, 5)
                                }
                            }
                        }.onChange(of: locationManager) { _ in
                            viewModel.locationChanged()
                        }
                    }
                    
                    VStack{
                        Panel(title:"Your reports", stats: $viewModel.userReportsStats)
                            .padding(.vertical, 5)
                        PanelCity(title:"Overall reports", viewModel: viewModel, stats: $viewModel.overallStats)
                            .padding(.vertical, 5)
                        Panel(title:"Your votes", stats: $viewModel.userVotesStats)
                            .padding(.vertical, 5)
                    }.padding(.all)
                }.onAppear{
                    viewModel.requestAll()
                }
            }.padding(.bottom, 20)
        }.onChange(of: networkMonitor.isConnected) { newValue in
            if newValue{
                viewModel.requestAll()
            }
        }
    }
    
}


struct DashboardReport:View{
    
    @Binding
    var reportImage: ReportImage
    
    @ObservedObject
    var viewModel: DashboardViewModel
    
    
    var body: some View{
        VStack{
            Image(uiImage: reportImage.image)
                .resizable()
                .scaledToFill()
                .frame(width: 200, height: 130, alignment: .topTrailing)
                .clipped()
            HStack{
                Image(systemName: "ruler.fill").foregroundColor(.yellow)
                Text(viewModel.getDistanceKm(from: reportImage.report.damageLocation))
            }
            HStack{
                Image(systemName: "exclamationmark.triangle.fill").foregroundColor(.yellow)
                Text(reportImage.report.severity != nil ? String(reportImage.report.severity!) : "N/A")
            }
            HStack{
                Image(systemName: "clock.fill").foregroundColor(.yellow)
                Text(reportImage.report.status != nil ? reportImage.report.status!.rawValue : "N/A")
            }
        }
        .padding(0.0)
        .frame(width: 200, height: 210, alignment: .center)
        .overlay(
            RoundedRectangle(cornerRadius: 15).stroke(Color.white, lineWidth: 0.5)
        )
        .background(
            RoundedRectangle(
                cornerRadius: 15
            )
                .foregroundColor(Color.white)
                .shadow(
                    color: Color.gray.opacity(0.4),
                    radius: 4,
                    x: 0,
                    y: 0
                )
        )
        
    }
}


struct PanelCity:View{
    
    let title: String
    
    @ObservedObject
    var viewModel: DashboardViewModel
    
    @Binding
    var stats: [String:String]
    
    @ObservedObject
    var networkMonitor = NetworkMonitor.singleton
    
    var body: some View{
        VStack{
            HStack{
                Text(title).bold().padding(.leading)
                
                Picker(viewModel.city, selection: $viewModel.city) {
                    ForEach(DashboardViewModel.cities, id: \.self) {
                        Text("\($0)")
                            .foregroundColor(.blue)
                    }
                }.pickerStyle(MenuPickerStyle()).frame(width: 80, alignment: .center).foregroundColor(networkMonitor.isConnected ? Color("PrimaryColor") : Color(red: 0.988, green: 0.4, blue: 0.263, opacity: 0.5)).onChange(of: viewModel.city) { _ in
                    viewModel.cityChanged()
                }.disabled(!networkMonitor.isConnected)
                
                
                Spacer()
            }
            ScrollView(.horizontal){
                HStack{
                    ForEach(stats.keys.sorted(), id: \.self) { key in
                        CircleStat(stat:stats[key]!, label:key)
                    }
                }
            }
            
        }.padding(.vertical)
        .overlay(
            RoundedRectangle(cornerRadius: 15)
                .stroke(Color.white, lineWidth: 0.1)
        )
        .background(
           RoundedRectangle(
             cornerRadius: 15
           )
           .foregroundColor(Color.white)
           .shadow(
             color: Color.gray.opacity(0.4),
             radius: 4,
             x: 0,
             y: 0
           )
        )
    
    }
}


struct Panel:View{
    
    let title: String
    
    @Binding
    var stats: [String:String]
    
    var body: some View{
        VStack{
            HStack{
                Text(title).bold().padding(.leading)
                Spacer()
            }
            ScrollView(.horizontal){
                HStack{
                    ForEach(stats.keys.sorted(), id: \.self) { key in
                        CircleStat(stat:stats[key]!, label:key)
                        
                    }
                }
            }
            
        }.padding(.vertical)
        .overlay(
            RoundedRectangle(cornerRadius: 15)
                .stroke(Color.white, lineWidth: 0.1)
        )
        .background(
           RoundedRectangle(
             cornerRadius: 15
           )
           .foregroundColor(Color.white)
           .shadow(
             color: Color.gray.opacity(0.4),
             radius: 4,
             x: 0,
             y: 0
           )
        )
    
    }
}


struct CircleStat: View{
    
    var stat: String
    
    var label: String
    
    let size: CGFloat = 65
    let hallow: Bool = false
    
    var body: some View{
        VStack{
            ZStack{
                if hallow{
                    Circle().fill(Color("PrimaryColor")).frame(width:size, height:size, alignment: .center)
                        .shadow(color: Color.gray.opacity(0.3), radius: 5)
                }else{
                    Circle().fill(Color("PrimaryColor")).frame(width:size, height:size, alignment: .center)
                        .shadow(color: Color.gray.opacity(0.3), radius: 5)
                }
                Text(stat)
                    .font(.title)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .frame(alignment: .center)
            }
            Text(label)
                .font(.caption)
                .frame(width: 80, height: 60, alignment: .center)
                .multilineTextAlignment(.center)
        }
    }
}

struct DashboardView_Previews: PreviewProvider {
    static var previews: some View {
        DashboardView()
    }
}
