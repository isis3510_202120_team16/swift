//
//  GoogleMapView.swift
//  FixUp
//
//  Created by Juan David Serrano on 7/10/21.
//

import SwiftUI
import GoogleMaps
import Firebase

struct GoogleMapViewBridge: UIViewRepresentable {
    
    @ObservedObject
    var viewModel: MapViewModel
    
    @EnvironmentObject
    var modalManager: ModalManager
    
    var userCircle = GMSCircle()
    
    var userLocation = GMSMarker()
    
    let markerRadius = 30.0
    
    typealias UIViewType = GMSMapView
    
    final class MapViewCoordinator: NSObject, GMSMapViewDelegate {
        var mapViewBridge: GoogleMapViewBridge
        
        init(_ mapViewBridge: GoogleMapViewBridge) {
            self.mapViewBridge = mapViewBridge
        }
        
        //        func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //            print(mapView, gesture)
        //            self.mapViewBridge.requestMapBoundVerification()
        //
        //        }
        
        func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
            guard let reportId = marker.title else { return true}
            self.mapViewBridge.showReportDetail(reportId: reportId)
            return true
        }
        
        func mapView(_ mapView: GMSMapView, idleAt cameraPosition: GMSCameraPosition) {
            self.mapViewBridge.requestMapBoundVerification()
            
        }
    }
    
    func requestMapBoundVerification(){
        viewModel.getReports(mapMoved:true)
        
    }
    
    func showReportDetail(reportId: String) {
        // search for report
        let searchReport = self.viewModel.reports.first(where: { $0.id == reportId })
        guard let report = searchReport else { return }
        
        // set modal content and show
        self.modalManager.showModal() {
            ReportDetailView(reportDetailViewModel: ReportDetailViewModel(report: report))
        }
        
        // Firebase Analytics
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: reportId,
            AnalyticsParameterContentType: report.type?.rawValue ?? "unknown",
            AnalyticsParameterItemCategory: report.category.rawValue,
        ])
    }
    
    func makeCoordinator() -> MapViewCoordinator {
        return MapViewCoordinator(self)
    }
    
    
    private func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    private func paintUser(_ mapView:GMSMapView){
        if viewModel.locationManager.region != nil {
            if !viewModel.atLeastOnceCentered, let userLoc = viewModel.locationManager.region?.center{
                mapView.animate(toLocation: userLoc)
                viewModel.atLeastOnceCentered = true
            }
            let userImage = imageWithImage(image: UIImage(systemName: "location.fill")!, scaledToSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
            let userView = UIImageView(image: userImage)
            userView.tintColor = UIColor(red: 0.9882, green: 0.4, blue: 0.26275, alpha: 1)
            userLocation.iconView = userView
            userLocation.position = viewModel.locationManager.region!.center
            userLocation.title = "User"
            userLocation.map = mapView
        }
        
    }
    
    private func paintArea(_ mapView:GMSMapView){
        if viewModel.withFilter && viewModel.locationManager.region != nil{
            mapView.animate(toLocation:viewModel.locationManager.region!.center)
            userCircle.position = viewModel.locationManager.region!.center
            userCircle.radius = viewModel.radius
            userCircle.map = mapView
            userCircle.fillColor = UIColor(red: 0.988, green: 0.4, blue: 0.263, alpha: 0.25)
            userCircle.strokeColor = UIColor.white
            userCircle.strokeWidth = 2
        }else if viewModel.withFilter{
            mapView.animate(toLocation:MapDetails.defaultLocation)
            userCircle.position = MapDetails.defaultLocation
            userCircle.radius = viewModel.radius
            userCircle.map = mapView
            userCircle.fillColor = UIColor(red: 0.988, green: 0.4, blue: 0.263, alpha: 0.25)
            userCircle.strokeColor = UIColor.white
            userCircle.strokeWidth = 2
        }
        
    }
    
    
    private func paintMarkers(_ mapView: GMSMapView){
        for report in viewModel.reports{
            let newMarker = GMSMarker(position: CLLocationCoordinate2D(latitude: report.damageLocation.latitude,
                                                                       longitude: report.damageLocation.longitude))
            let markerImage = self.imageWithImage(image: UIImage(systemName: "circle.fill")!, scaledToSize: CGSize(width: 30.0, height: 30.0)).withRenderingMode(.alwaysTemplate)
            let markerView = UIImageView(image: markerImage)
            markerView.tintColor = UIColor(viewModel.getReportMarkerColor( report.category ))
            newMarker.iconView = markerView
            newMarker.title = report.id
            newMarker.map = mapView
        }
        
    }
    
    func makeUIView(context: Context) -> GMSMapView {
        let mapView = viewModel.mapView
        
        do {
            if let styleURL = Bundle.main.url(forResource: "style", withExtension: "json") {
                mapView.mapStyle = try GMSMapStyle(contentsOfFileURL: styleURL)
            } else {
                NSLog("Unable to find style.json")
            }
            
        } catch {
            NSLog("One or more of the map styles failed to load. \(error)")
        }
        
        // Not working button on map
//        mapView.settings.myLocationButton = true
        mapView.delegate = context.coordinator
        paintUser(mapView)
        paintArea(mapView)
        paintMarkers(mapView)
        return mapView
    }
    
    func updateUIView(_ mapView: GMSMapView, context: Context) {
        mapView.clear()
        paintArea(mapView)
        paintUser(mapView)
        paintMarkers(mapView)
    }
    
}



struct GoogleMapView: View {
    
    @ObservedObject
    var viewModel = MapViewModel()
    
    @State
    var previousToggle = false
    
    @ObservedObject
    var networkMonitor:NetworkMonitor = NetworkMonitor.singleton
    
    @ObservedObject
    var locationManager = LocationManager.singleton
    
    
    var body: some View {
        
        NavigationView{
            ZStack{
                GoogleMapViewBridge(viewModel: viewModel)
                    .ignoresSafeArea().onChange(of: locationManager) { _ in
                        viewModel.getReports(mapMoved: false)
                    }.onChange(of: networkMonitor.isConnected) { _ in
                        if !networkMonitor.isConnected{
                            viewModel.getReports(mapMoved: false)
                        }
                    }
                VStack{
                    HStack{
                        Spacer()
                        if viewModel.withFilter && viewModel.reports.count > 0 {
                            VStack{
                                if viewModel.locationManager.region == nil{
                                    Text("Using Uniandes as filter location center").foregroundColor(Color.white).background(Color.gray).frame(maxWidth: .infinity, alignment: .center)
                                }
                                Text("Most Voted: " + viewModel.reports[0].category.rawValue).foregroundColor(Color("PrimaryColor")).background(Color.white).frame(maxWidth: .infinity, alignment: .center)
                            }
                        }else if !networkMonitor.isConnected && !viewModel.withFilter{
                            
                            Snackbar(label: "No internet: filter disabled and showing only vecinity reports.")
                            
                        }
                        Spacer()
                    }
                    Spacer()
                    if viewModel.withFilter{
                        Slider(value: $viewModel.radius, in: 0...MapViewModel.maxRadius){
                            editing in
                            if !editing{
                                viewModel.getReports(mapMoved: false)
                            }
                        }
                        
                        .padding()
                        .accentColor(.red)
                    }
                    
                }
                .padding(.all)
                
            }.navigationBarTitle(Text("Map"), displayMode: .inline)
            .toolbar {
                Toggle("Filter", isOn: $viewModel.withFilter).disabled(!networkMonitor.isConnected).onChange(of: viewModel.withFilter) { param in
                    viewModel.getReports(mapMoved: false)
                    if (param) { Analytics.logEvent(AnalyticsEventActivateFilter, parameters: nil) }
                }
            }
        }.onAppear{
            viewModel.locationManager.checkIfLocationServicesIsEnabled()
            
        }
    }
}

struct GoogleMapView_Previews: PreviewProvider {
    static var previews: some View {
        GoogleMapView()
    }
}


struct ReportMarker{
    let color:Color
    let position: CLLocationCoordinate2D
}
