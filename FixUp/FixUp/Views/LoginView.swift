//
//  LoginView.swift
//  FixUp
//
//  Created by Camila Pantoja on 5/10/21.
//

import SwiftUI

struct LoginView: View {
    
    @State var email = ""
    @State var password = ""
    
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    @ObservedObject var monitor = NetworkMonitor.singleton
    //@State private var showNotConnectionAlert = false
    
    
    
    var body: some View {
        
        NavigationView{
        VStack {
            
            Image("FixUpLogo")
                .padding(35)
                .padding(.top, 40)
            
            Group {
                Text("LOGIN")
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .foregroundColor(Color("PrimaryColor"))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.horizontal)
                
                Text("Email")
                    .foregroundColor(Color(.secondaryLabel))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding()
                
                TextField("", text: $email)
                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                    .padding(.horizontal)
                    .autocapitalization(UITextAutocapitalizationType.none)
                    .disableAutocorrection(true)
                
                Text("Password")
                    .foregroundColor(Color(.secondaryLabel))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding()
                
                SecureField("", text: $password)
                    .overlay(Divider().background(Color(.label)), alignment: .bottom)
                    .padding(.horizontal)
                
                Text("Forgot password?")
                    .font(.caption)
                    .foregroundColor(Color(.tertiaryLabel))
                    .frame(maxWidth: .infinity, alignment: .trailing)
                    .padding(.horizontal, 35)
            }
            .alert(isPresented: $loginViewModel.errorOcurred,
                   content: {
                    
                    if(!monitor.isConnected){
                        return Alert(title: Text("No Connection"), message: Text("You are offline. Please check your internet connection and try later."))
                    }
                    else{
                        return Alert(title: Text("Error"), message: Text(loginViewModel.errorMessage))
                    }
                   })
            /*.alert(isPresented: $showNotConnectionAlert,
                   content: {
                    return Alert(title: Text("Error"), message: Text("No Connection"))
                   })
            */
            Button(action: {
                guard !email.isEmpty, !password.isEmpty else {
                    return
                }
                /*if !monitor.isConnected{
                    showNotConnectionAlert = true
                }*/
                loginViewModel.signIn(email: email, password: password)
            }) {
                Text("LOGIN")
                    .foregroundColor(.white)
                    .font(.system(size: 20, weight: .medium))
                    .padding(.vertical, 10)
                    .frame(maxWidth: .infinity)
                    .background(Color("PrimaryColor"))
                    .cornerRadius(8)
                    .padding()
            }
            
            Spacer()
            
            // TODO Navigation Link          
            Text("Don't have an account? Sign up")
                .font(.caption)
                .foregroundColor(Color(.secondaryLabel))
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.vertical, 10)
            
        }
        .background(LoginBackground())
        .edgesIgnoringSafeArea(.all)
        }
        .navigationBarBackButtonHidden(true)
    }
}

struct LoginBackground: View {
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                path.move(to: CGPoint(x: 0, y: geometry.size.height))
                path.addLine(to: CGPoint(x: geometry.size.width, y: geometry.size.height * 0.66))
                path.addLine(to: CGPoint(x: geometry.size.width, y: geometry.size.height))
                path.addLine(to: CGPoint(x: 0, y: geometry.size.height))
            }.fill(Color.white)
        }.background(Color("SecondaryColor"))
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
