////
////  MapAreaView.swift
////  FixUp
////
////  Created by Juan David Serrano on 5/10/21.
////
//
import SwiftUI
import MapKit

//let mapView = MKMapView()
//
//struct MapAreaView: UIViewRepresentable {
//    
//    @Binding var region: MKCoordinateRegion
//    
//    func makeUIView(context: Context) -> MKMapView {
//        mapView.setRegion(region, animated: false)
//        mapView.delegate = context.coordinator
//        mapView.showsUserLocation = true
//        return mapView
//    }
//    
//    func updateUIView(_ uiView: MKMapView, context: Context) {
//        
//    }
//    
//    class Coordinator: NSObject, MKMapViewDelegate {
//        var parent: MapAreaView
//        
//        init(_ parent: MapAreaView) {
//            self.parent = parent
//        }
//        
//        func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//            if overlay is MKCircle{
//                print("Entro al circulo")
//                let circleView = MKCircleRenderer(overlay: overlay)
//                circleView.strokeColor = .red
//                return circleView
//            }
//            return MKOverlayRenderer()
//        }
//        
//        func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//            
//            return MKAnnotationView()
//        }
//        
//        // Se llama cada vez que se mueve el mapa
//        func mapViewDidChangeVisibleRegion(_ mapView: MKMapView) {
//            parent.region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 4.65783, longitude: -74.01792), span: MapDetails.defaultSpan)
//        }
//    }
//    
//    func makeCoordinator() -> Coordinator {
//        Coordinator(self)
//    }
//}
//
//struct MapGeneralView: View {
//    @State var mapOverlay = false
//    @State var radius:Double = 10
//    
//    @StateObject private var viewModel = LocationManager.singleton
//    
//    var body: some View {
//        NavigationView {
//            MapAreaView(region:$viewModel.region)
//                .navigationBarTitle("", displayMode: .inline)
//                .navigationBarItems(leading:
//                                        HStack {
//                                            Button(":Overlay:") {
//                                                self.mapOverlay.toggle()
//                                                self.updateMapOverlayViews()
//                                            }
//                                            .foregroundColor(mapOverlay ? .white : .red)
//                                            .background(mapOverlay ? Color.green : Color.clear)
//                                        }
//                )
//                .ignoresSafeArea()
//                .accentColor(Color(.systemOrange))
//                .onAppear{
//                    viewModel.checkIfLocationServicesIsEnabled()
//                }
//        }
//    }
//    
//    func addOverlay() {
//        let overlay = MKCircle(center: MapDetails.defaultLocation, radius: radius)
//        mapView.addOverlay(overlay)
//    }
//    
//    func updateMapOverlayViews() {
//        mapView.removeAnnotations(mapView.annotations)
//        mapView.removeOverlays(mapView.overlays)
//        if mapOverlay { addOverlay() }
//    }
//}
