//
//  MapView.swift
//  FixUp
//
//  Created by Juan David Serrano on 5/10/21.
//

import SwiftUI
import MapKit

struct MapView: View {
    @StateObject private var viewModel = LocationManager.singleton
    
    let annotations = [
        Annotation(center: CLLocationCoordinate2D(latitude: 4.60383, longitude: -74.06625)),
        Annotation(center: CLLocationCoordinate2D(latitude: 4.60258, longitude: -74.06676)),
        Annotation(center: CLLocationCoordinate2D(latitude: 4.60301, longitude: -74.06385)),
    ]
    
    
    var body: some View {
        Map(coordinateRegion: $viewModel.region, showsUserLocation: true, annotationItems:annotations){
            annotation in
            MapPin(coordinate: annotation.center, tint: Color.purple)
        }
        .ignoresSafeArea()
        .accentColor(Color(.systemOrange))
        .onAppear{
            viewModel.checkIfLocationServicesIsEnabled()
        }
    }
    
}


struct Annotation: Identifiable{
    let id: UUID = UUID()
    let center: CLLocationCoordinate2D
    init(center: CLLocationCoordinate2D){
        self.center = center
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}

