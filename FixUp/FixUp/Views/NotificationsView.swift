//
//  NotificationsView.swift
//  FixUp
//
//  Created by Camila Pantoja on 11/11/21.
//

import SwiftUI

struct NotificationsView: View {
    @EnvironmentObject var modalManager: ModalManager
    
    var body: some View {
        Text("Notifications view (TBA)").font(.title)
    }
}

struct NotificationsView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationsView()
    }
}
