//
//  ProfileView.swift
//  FixUp
//
//  Created by Camila Pantoja on 6/10/21.
//

import SwiftUI

import FirebaseAuth

struct ProfileView: View {
    
    @EnvironmentObject var loginViewModel: LoginViewModel
    
    var body: some View {
        VStack{
            UserRegion()
            ButtonsRegion()
            Spacer()
            Button(action: {
                loginViewModel.signOut()
            }) {
                Text("LOG OUT")
                    .foregroundColor(.white)
                    .font(.system(size: 20, weight: .medium))
                    .padding(.vertical, 10)
                    .frame(maxWidth: .infinity)
                    .background(Color("PrimaryColor"))
                    .cornerRadius(8)
                    .padding()
            }.padding(.bottom, 30)
        }.ignoresSafeArea(.all)
        
    }
}


struct UserRegion: View{
    
    
    let circleSize: CGFloat = 150
    
    let user = Auth.auth().currentUser
    
    var body: some View{
        VStack{
            Spacer()
            Image(user?.photoURL?.absoluteString ?? "MaleAvatar")
                        .resizable()
                        .frame(width: circleSize, height: circleSize)
                        .clipShape(Circle())
            Text(user?.displayName ?? "No name: Error during creation")
                .font(.title2)
                .padding(.top)
            Text(verbatim: user?.email ?? "No email: shouldn't have happened")
                .font(.headline)
                .padding(0.0)
            Spacer()
        }.frame(maxWidth: .infinity, maxHeight:320).background(Color("SecondaryColor")).padding(.bottom, 0)
    }
}


struct ButtonsRegion: View{
    var body: some View{
        NormalButton(text: "My Reports", iconName:"exclamationmark.octagon.fill").padding(.top, 10)
        Divider()
        NormalButton(text: "Invite friends", iconName:"bell.fill")
        Divider()
        NormalLabel(paramText: "About", paramIconName:"info.circle.fill", paramTrailingText: "0.0.1")
        Divider()
    }
}


struct NormalButton:View{
    
    let text:String
    
    let iconName: String
    
    var body: some View{
        Button(action: {
        }) {
            NormalLabel(paramText: text, paramIconName:iconName)
        }
    }
}

struct NormalLabel:View{
    
    let text: String
    
    let iconName: String
    
    let trailingText: String?
    
    init(paramText : String, paramIconName : String, paramTrailingText : String? = nil){
        self.text = paramText
        self.iconName = paramIconName
        self.trailingText = paramTrailingText
    }
    
    var body: some View{
        
        HStack{
            Image(systemName: iconName).foregroundColor(Color("PrimaryColor")).padding(.horizontal, 6.0)
            Text(text)
                .foregroundColor(.black)
                .font(.system(size: 20, weight: .medium))
            Spacer()
            if trailingText != nil{
                Text(trailingText!)
                    .foregroundColor(.black)
                    .font(.system(size: 20, weight: .medium))
                    .padding(.trailing, 10)
            }
        }.frame(height: 30)
        
    }
}


struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
    }
}
