//
//  RatingView.swift
//  FixUp
//
//  Created by Daniela González on 5/10/21.
//

import SwiftUI


struct RatingView: View {
    
    @Binding var rating: Int?

    var label = ""

    var maximumRating = 3

    var offImage: Image?
    var onImage = Image(systemName: "circle.fill")

    var offColor = Color(red: 0.9608, green: 0.9608, blue: 0.9608)
    var onColor = Color("PrimaryColor")
    
    func image(for number: Int) -> Image {
        if number > (rating ?? 1) {
            return offImage ?? onImage
        } else {
            return onImage
        }
    }
    
    var body: some View {
        HStack {
            if label.isEmpty == false {
                Text(label)
            }

            ForEach(1..<maximumRating + 1) { number in
                self.image(for: number)
                    .resizable()
                    .frame(width: 34.0, height: 34.0)
                    .foregroundColor(number > (self.rating ?? 1) ? self.offColor : self.onColor)
                    .onTapGesture {
                        self.rating = number
                    }
                
            }
            .shadow(color: Color.gray.opacity(0.3), radius: 4)
            .padding(.horizontal, 25)
        }
    }
}

struct RatingView_Previews: PreviewProvider {
    static var previews: some View {
        RatingView(rating: .constant(2))
    }
}
