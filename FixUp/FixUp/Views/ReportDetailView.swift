//
//  ReportDetailView.swift
//  FixUp
//
//  Created by Camila Pantoja on 3/11/21.
//

import SwiftUI
import CoreLocation
import MobileCoreServices
import Firebase

struct ReportDetailView: View {
    @ObservedObject var locationManager: LocationManager = LocationManager.singleton
    
    @ObservedObject var reportDetailViewModel: ReportDetailViewModel
    
    @ObservedObject var monitor = NetworkMonitor.singleton
    
    // Variables for no connection alerts
    @State private var showNotConnectionAlert = false
    @State private var noConnectionMessage = ""
    
    //@State private var canVoteBool = false
    
    func formatDistance(distance: Double) -> String {
        if distance > 1000 {
            return String(format: "%.1f km", distance/1000)
        } else {
            return "\(Int(distance)) m"
        }
    }
    
    func currentLocation() -> CLLocation {
        let userLocation = locationManager.region?.center ?? MapDetails.defaultLocation
        return CLLocation(latitude: userLocation.latitude, longitude: userLocation.longitude)
    }
    
    func getSharingItems() -> [Any]? {
        var items = [Any]()
        
        // Share text description
        let shareString = reportDetailViewModel.report.type == ReportType.Damage ? "Watch out for a \(reportDetailViewModel.report.category.rawValue) \(reportDetailViewModel.report.type!.rawValue) in this location" : "Check for a \(reportDetailViewModel.report.category.rawValue) \(reportDetailViewModel.report.type!.rawValue) in this location"
        items.append(shareString)
        
        // Share link to Google Maps location
        let URLString = "https://maps.google.com/maps?q=\(reportDetailViewModel.report.damageLocation.latitude)%2C\(reportDetailViewModel.report.damageLocation.longitude)"
        if let url = NSURL(string: URLString) {
            items.append(url)
        }
        
        // Share description if available
        if (reportDetailViewModel.report.description != nil) && reportDetailViewModel.report.description != "" {
            items.append("Description: \(reportDetailViewModel.report.description ?? "")")
        }
        
        
        // Some apps allow also image exporting
        if(reportDetailViewModel.hasMedia) {
            items.append(reportDetailViewModel.downloadedMedia)
        }
        
        return items
    }
    
    func shareSheet() {
        if(!monitor.isConnected){
            showNotConnectionAlert = true
            noConnectionMessage = "You can not share a report while offline. Please check your internet connection and try later."
        } else {
            showNotConnectionAlert = false
            guard let sharingItems = getSharingItems() else {
                return
            }
            print("items to share: ", sharingItems)
            
            let activityVC = UIActivityViewController(activityItems: sharingItems, applicationActivities: nil)
            UIApplication.shared.windows.first?.rootViewController?.present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = {activity, completed, items, error in
                if completed {
                    Analytics.logEvent(AnalyticsEventShare, parameters: [
                        AnalyticsParameterItemID: reportDetailViewModel.report.id ?? "unknown",
                        AnalyticsParameterMethod: activity ?? "unknown",
                    ])
                }
            }
        }
    }
    
    func canVote() -> Bool {
        
        let userID = reportDetailViewModel.uid
        //var usersVotes = reportDetailViewModel.usersRepository.$usersVotes
        var boolResp = true
        
        //print(reportDetailViewModel.usersRepository.usersVotes[0])
        for user in reportDetailViewModel.usersRepository.usersVotes{
            if user.id == userID{
                //user.votedReports.first(where: {$0.id == userID})
                for reportVoted in user.votedReports{
                    if(reportVoted == reportDetailViewModel.report.id){
                        boolResp = false
                    }
                }
            }
        }
        
        return boolResp
    }
    
    func vote(){
        
        if(!monitor.isConnected){
            showNotConnectionAlert = true
            noConnectionMessage = "You can not vote for any report while offline. Please check your internet connection and try later."
        }
        else{
            showNotConnectionAlert = false
            let userID = reportDetailViewModel.uid
            let newVote = reportDetailViewModel.report.id
            
            var newVotedReports: [String] = []
            newVotedReports.append(newVote!)
            
            let newUsersVotes = UsersVotes( id: userID, votedReports: newVotedReports)
            
            // print(reportDetailViewModel.usersRepository.usersVotes.first(where: {$0.id == userID}))
            
            var createNew = true
            
            for user in reportDetailViewModel.usersRepository.usersVotes{
                if user.id == userID{
                    createNew = false
                }
            }
            
            if(createNew == true){
                reportDetailViewModel.addVoteNewUser(usersVotes: newUsersVotes)
                reportDetailViewModel.addVoteReport(report: reportDetailViewModel.report)
                //reportDetailViewModel.report.numVotes = reportDetailViewModel.report.numVotes! + 1
                
                // Firebase event
                Analytics.logEvent(AnalyticsEventVote, parameters: [
                    AnalyticsParameterItemID: reportDetailViewModel.report.id ?? "unknown"
                ])
            } else{
                reportDetailViewModel.addVoteExistingUser(pUserId: userID, pReportId: newVote!)
                reportDetailViewModel.addVoteReport(report: reportDetailViewModel.report)
                //reportDetailViewModel.report.numVotes = reportDetailViewModel.report.numVotes! + 1
                
                // Firebase event
                Analytics.logEvent(AnalyticsEventVote, parameters: [
                    AnalyticsParameterItemID: reportDetailViewModel.report.id ?? "unknown"
                ])
            }
        }
    }
    
    var body: some View {
        ScrollView{
            VStack(alignment: .leading, spacing: 10) {
                
                Text("\(reportDetailViewModel.report.category.rawValue) \(reportDetailViewModel.report.type!.rawValue)")
                    .font(.title)
                    .fontWeight(/*@START_MENU_TOKEN@*/.bold/*@END_MENU_TOKEN@*/)
                
                HStack(alignment: .top){
                    // Badge
                    CategoryBadgeView(label: reportDetailViewModel.report.category.rawValue, color: Color(reportDetailViewModel.report.category.getColorKey()))
                    // Distance indicator
                    Text(formatDistance(distance: reportDetailViewModel.getDistance(from: currentLocation())))
                    
                    Spacer()
                    
                    // Buttons
                    Group {
                        // Like button
                        DetailButton(iconSystemName: "hand.thumbsup.fill", action: vote, caption: "(\(reportDetailViewModel.report.numVotes ?? 0))", disabled: reportDetailViewModel.userHasVoted)
                        
                        // Share button
                        DetailButton(iconSystemName: "square.and.arrow.up", action: shareSheet)
                    }
                    .padding(10)
                }
                
                Group {
                    DetailSection(label: "Severity"){
                        RatingView(rating: .constant( reportDetailViewModel.report.severity! ))
                    }
                    
                    DetailSection(label: "Status"){
                        Text(reportDetailViewModel.report.status!.getDetailMeaning())
                    }
                    DetailSection(label: "Photo"){
                        Image(uiImage: reportDetailViewModel.downloadedMedia)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 200, height: 200)
                            .clipped()
                    }
                    DetailSection(label: "Description"){
                        Text(reportDetailViewModel.report.description!)
                    }
                }
                Spacer()
            }
            .padding()
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: .leading)
            .alert(isPresented: $showNotConnectionAlert,
                   content: {
                return Alert(title: Text("No Connection"), message: Text(noConnectionMessage))
            })
        }
        
    }
    
    
}

struct ReportDetailView_Previews: PreviewProvider {
    static var previews: some View {
        ReportDetailView(reportDetailViewModel: ReportDetailViewModel(report: Report(author: "author", category: .Road, city: "Bogotá", country: "Colombia", creationLocation: Location(latitude: 4.645964, longitude: -74.058490), damageLocation: Location(latitude: 4.645964, longitude: -74.058490), description: "Huge pothole filled with water", media: Media(altName: "pothole", path: "path...", type: .PHOTO), numVotes: 1001, severity: 3, status: .Active, type: .Damage)))
    }
}

struct CategoryBadgeView: View {
    var label: String
    var color: Color = .blue
    
    var body: some View {
        HStack {
            Text(label)
                .font(.caption)
                .foregroundColor(.white)
        }
        .padding(.horizontal, 10)
        .padding(.vertical, 5)
        .background(color)
        .cornerRadius(8)
    }
}


struct DetailSection<Content:View>: View {
    var label: String
    
    var content: () -> Content
    
    init(label: String, @ViewBuilder content: @escaping () -> Content) {
        self.label = label
        self.content = content
    }
    
    var body: some View {
        Text(label)
            .font(.headline)
        content()
            .frame(maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
    }
}
