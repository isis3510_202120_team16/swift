//
//  SUImagePickerView.swift
//  FixUp
//
//  Created by Daniela González on 5/10/21.
//

import SwiftUI
import UIKit
import Firebase

struct SUImagePickerView: UIViewControllerRepresentable {
    
    var sourceType: UIImagePickerController.SourceType = .photoLibrary
    @Binding var image: Image?
    @Binding var uiImage: UIImage?
    @Binding var isPresented: Bool
    
    func makeCoordinator() -> ImagePickerViewCoordinator {
        return ImagePickerViewCoordinator(image: $image, uiImage: $uiImage, isPresented: $isPresented)
    }
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let pickerController = UIImagePickerController()
        pickerController.sourceType = sourceType
        pickerController.delegate = context.coordinator
        return pickerController
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
        // Nothing to update here
    }
    
}

class ImagePickerViewCoordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @Binding var image: Image?
    @Binding var uiImage: UIImage?
    @Binding var isPresented: Bool
    
    init(image: Binding<Image?>, uiImage: Binding<UIImage?>, isPresented: Binding<Bool>) {
        self._image = image
        self._uiImage = uiImage
        self._isPresented = isPresented
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.uiImage = image
            self.image = Image(uiImage: image)
            
            if (picker.sourceType == UIImagePickerController.SourceType.camera) {
                // If photo was taken, save to camera roll
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                // Add Firebase event
                Analytics.logEvent(AnalyticsEventCreateReportPhoto, parameters: [AnalyticsParameterMethod: "camera"])
            } else {
                // Add Firebase event
                Analytics.logEvent(AnalyticsEventCreateReportPhoto, parameters: [AnalyticsParameterMethod: "library"])
            }
        }
        self.isPresented = false
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.isPresented = false
    }
    
    
}

/*struct SUImagePickerView_Previews: PreviewProvider {
 static var previews: some View {
 SUImagePickerView()
 }
 }*/
