//
//  SelectLocationView.swift
//  FixUp
//
//  Created by Daniela González on 6/10/21.
//

import MapKit
import SwiftUI
import Firebase

struct MyAnnotationItem: Identifiable {
    var coordinate: CLLocationCoordinate2D
    let id = UUID()
}


struct SelectLocationView: View {
    
    @StateObject private var viewModel = LocationManager.singleton
    
    @State private var directions: String = ""
    
    @Binding
    var chosenLat: CLLocationDegrees
    
    @Binding
    var chosenLong: CLLocationDegrees
    
    @State
    var reportLocation: MKCoordinateRegion
    
    /*@Binding var damagePersistedLatitude: Double
    @Binding var damagePersistedLongitude: Double*/
    
    /*@State private var selectedLo = SelectLocationViewModel().region.center*/

    
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var body: some View {
        
       
        /*let coordinate = self.viewModel.location != nil ? self.viewModel.location!.coordinate : CLLocationCoordinate2D()*/
//        let lat = self.viewModel.region?.center.latitude ?? MapDetails.defaultLocation.latitude
//        let long = self.viewModel.region?.center.longitude ?? MapDetails.defaultLocation.longitude
        
        
        
        let annotationItems: [MyAnnotationItem] = [
            MyAnnotationItem(coordinate: CLLocationCoordinate2D(latitude: reportLocation.center.latitude, longitude: reportLocation.center.longitude))
        ]
        
        
        VStack{
            Text("Location")
                .fontWeight(.semibold)
                .padding(.top, -35)
            List{
                Section(header: Text("CHOOSE LOCATION WITH MAP PIN")){
                    Text("\(reportLocation.center.latitude), \(reportLocation.center.longitude)")
                }
            }
            .listStyle(InsetGroupedListStyle())
            .frame(width: 410, height: 110, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            
            
            Button(action: {
                
                chosenLat = reportLocation.center.latitude
                chosenLong = reportLocation.center.longitude
                
                // Add Firebase event
                Analytics.logEvent(AnalyticsEventCreateReportCustomLocation, parameters: nil)
                
                self.mode.wrappedValue.dismiss()
            }) {
                Text("Select location")
            }
            
            Map(coordinateRegion: $reportLocation , showsUserLocation: true,
                annotationItems: annotationItems) {item in
                MapPin(coordinate: item.coordinate)
                }
                    .ignoresSafeArea()
                    .accentColor(Color(red: 0.9882, green: 0.4, blue: 0.26275))
                    .onAppear{
                        viewModel.checkIfLocationServicesIsEnabled()
                    }
            
        }
    }
}

//struct SelectLocationView_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectLocationView()
//    }
//}
