//
//  SignUpView.swift
//  FixUp
//
//  Created by Daniela González on 2/12/21.
//

import SwiftUI

struct SignUpView: View {
    
    @State var firstName = ""
    @State var lastName = ""
    @State var email = ""
    @State var password = ""
    @State var repeatedPassword = ""
    
    var body: some View {
        NavigationView{
        VStack {
            
            Group {
                
                Text("SIGN UP")
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .foregroundColor(Color("PrimaryColor"))
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.top, 120)
                    .padding(.bottom, 10)
                    .padding(.horizontal)
                
                Group {
                    Text("First Name")
                        .foregroundColor(Color(.secondaryLabel))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    TextField("", text: $firstName)
                        .overlay(Divider().background(Color(.label)), alignment: .bottom)
                        .padding(.horizontal)
                        .autocapitalization(UITextAutocapitalizationType.none)
                        .disableAutocorrection(true)
                }
                
                Group {
                    Text("Last Name")
                        .foregroundColor(Color(.secondaryLabel))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    TextField("", text: $lastName)
                        .overlay(Divider().background(Color(.label)), alignment: .bottom)
                        .padding(.horizontal)
                        .autocapitalization(UITextAutocapitalizationType.none)
                        .disableAutocorrection(true)
                }
                
                Group {
                    Text("Email")
                        .foregroundColor(Color(.secondaryLabel))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    TextField("", text: $email)
                        .overlay(Divider().background(Color(.label)), alignment: .bottom)
                        .padding(.horizontal)
                        .autocapitalization(UITextAutocapitalizationType.none)
                        .disableAutocorrection(true)
                }
                
                Group {
                    Text("Password")
                        .foregroundColor(Color(.secondaryLabel))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    SecureField("", text: $password)
                        .overlay(Divider().background(Color(.label)), alignment: .bottom)
                        .padding(.horizontal)
                }
                
                Group {
                    Text("Repeat Password")
                        .foregroundColor(Color(.secondaryLabel))
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .padding()
                    
                    SecureField("", text: $repeatedPassword)
                        .overlay(Divider().background(Color(.label)), alignment: .bottom)
                        .padding(.horizontal)
                }
            }
            /*.alert(isPresented: $loginViewModel.errorOcurred,
                   content: {
                    
                    if(!monitor.isConnected){
                        return Alert(title: Text("No Connection"), message: Text("You are offline. Please check your internet connection and try later."))
                    }
                    else{
                        return Alert(title: Text("Error"), message: Text(loginViewModel.errorMessage))
                    }
                   })
            /*.alert(isPresented: $showNotConnectionAlert,
                   content: {
                    return Alert(title: Text("Error"), message: Text("No Connection"))
                   })
            */*/
            Button(action: {
                guard !firstName.isEmpty, !lastName.isEmpty, !email.isEmpty, !password.isEmpty, !repeatedPassword.isEmpty, password==repeatedPassword else {
                    return
                }
                /*if !monitor.isConnected{
                    showNotConnectionAlert = true
                }*/
                //loginViewModel.signIn(email: email, password: password)
            }) {
                Text("SIGN UP")
                    .foregroundColor(.white)
                    .font(.system(size: 20, weight: .medium))
                    .padding(.vertical, 10)
                    .frame(maxWidth: .infinity)
                    .background(Color("PrimaryColor"))
                    .cornerRadius(8)
                    .padding()
            }
            .padding(.top, 20)
            
            //Spacer()
            
            HStack {
                Text("Already have an account?")
                    .foregroundColor(Color(.secondaryLabel))
                    .frame(maxWidth: .infinity, alignment: .center)
                NavigationLink(destination: LoginView()) {
                    Text("Login")
                }
            }
            .padding(.top, 60)
            
            /*Text("Don't have an account? Sign up")
                .font(.caption)
                .foregroundColor(Color(.secondaryLabel))
                .frame(maxWidth: .infinity, alignment: .center)
                .padding(.vertical, 10)*/
            
            Spacer()
            
        }
        .background(SignUpBackground())
        .edgesIgnoringSafeArea(.all)
        }
        .navigationBarBackButtonHidden(true)
        
    }
}

struct SignUpBackground: View {
    var body: some View {
        GeometryReader { geometry in
            Path { path in
                path.move(to: CGPoint(x: 0, y: 0))
                path.addLine(to: CGPoint(x: geometry.size.width, y: geometry.size.height * 0.25))
                path.addLine(to: CGPoint(x: geometry.size.width, y:0))
                path.addLine(to: CGPoint(x: 0, y: 0))
            }.fill(Color.white)
        }.background(Color("SecondaryColor"))
    }
}

struct SignUpView_Previews: PreviewProvider {
    static var previews: some View {
        SignUpView()
    }
}
