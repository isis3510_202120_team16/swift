//
//  TabBarView.swift
//  FixUp
//
//  Created by Camila Pantoja on 5/10/21.
//

import SwiftUI
import Firebase

struct TabBarView: View {
    
    @ObservedObject var router = ViewRouter()
    @State var showModal = false
    
    let tabBarHeight = UIScreen.main.bounds.height * 0.0875
    
    var body: some View {
        ZStack {
            VStack (spacing: 0) {
                // Views
                router.view
                    .frame(maxHeight: .infinity)
                
                // Custom TabBar
                HStack {
                    TabIcon(viewModel: .profile, router: router, baseHeight: tabBarHeight)
                    TabIcon(viewModel: .dashboard, router: router, baseHeight: tabBarHeight)
                    TabCenterFAB(baseHeight: tabBarHeight).onTapGesture {
                        showModal.toggle()
                        
                        // Send Firebase Event
                        Analytics.logEvent(AnalyticsEventScreenCreateReport, parameters: nil)
                    }
                    TabIcon(viewModel: .map, router: router, baseHeight: tabBarHeight)
                    TabIcon(viewModel: .notifications, router: router, baseHeight: tabBarHeight)
                }
                .frame(height: tabBarHeight)
                .frame(maxWidth: .infinity, alignment: .bottom)
                .background(
                    Rectangle()
                        .foregroundColor(Color.white)
                        .customCornerRadius(12, corners: [.topLeft, .topRight])
                        .shadow(
                            color: Color.black.opacity(0.18),
                            radius: 15,
                            x: 0,
                            y: 1
                        )
                )
            }.edgesIgnoringSafeArea(.bottom)
            
            ModalView(isShowing: $showModal) {
                CreateReportView(showModal: $showModal)
            }
        }
    }
}

struct TabCenterFAB: View {
    let baseHeight: CGFloat
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: /*@START_MENU_TOKEN@*/25.0/*@END_MENU_TOKEN@*/)
                .foregroundColor(Color("PrimaryColor"))
                .frame(width: baseHeight, height: baseHeight)
                .shadow(
                    color: Color.black.opacity(0.15),
                    radius: 15,
                    x: 0,
                    y: 1
                )
            Image(systemName: "plus")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: baseHeight * 0.3, height: baseHeight * 0.3)
                .foregroundColor(.white)
        }
        .offset(y: -baseHeight*0.5)
    }
}

struct TabIcon: View {
    let viewModel: TabBarViewModel
    @ObservedObject var router: ViewRouter
    
    let baseHeight: CGFloat
    
    var body: some View {
        Button(action: {
            router.currentItem = viewModel
            
            // Send Firebase event
            Analytics.logEvent(viewModel.analyticsEventName, parameters: nil)
            
        }, label: {
            Spacer()
            Image(systemName: viewModel.imageName)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: baseHeight * 0.5, height: baseHeight * 0.5 )
                .font(.system(size: 24, weight: .bold))
                .foregroundColor(router.currentItem == viewModel ? Color("PrimaryColor") : Color(.secondaryLabel))
            Spacer()
        })
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TabBarView()
            TabBarView()
        }
    }
}
